;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (scmackerel processes))

(define void
  (sm:type (name "Void") (entities '("void"))))

(define ihello'actions
  (sm:type (name "ihello'actions")
           (entities '("ihello'in'hello"))))

(define ihello'modeling
  (sm:type (name "ihello'modeling")
           (entities '("ihello'optional" "ihello'inevitable"))))

(define ihello'replies
  (sm:type (name "ihello'replies")
           (entities (list
                      (sm:predicate (name "ihello'nil_is")
                                    (type "ihello'nil"))
                      (sm:entity (name "ihello'Void")
                                 (formals
                                  (list (sm:formal (type void)
                                                   (name "value")))))))))

(define ihello'events
  (sm:type (name "ihello'events")
           (entities (list (sm:entity (name "ihello'action")
                                      (formals
                                       (list (sm:formal (type ihello'actions)
                                                        (name "value")))))
                           (sm:entity (name "ihello'reply")
                                      (formals
                                       (list (sm:formal (type ihello'replies)
                                                        (name "value")))))))))

(define ihello'in
  (sm:action (prefix "ihello'in")
             (event (sm:event (constructor "ihello'action")
                              (type ihello'events)
                              (arguments '("ihello'in'hello"))))))

(define ihello'internal
  (sm:action (prefix "ihello'internal")
             (event (sm:event (constructor "ihello'modeling")
                              (type ihello'modeling)))))

(define ihello'reply
  (sm:action (prefix "ihello'reply")
             (event (sm:event (constructor "ihello'Void")
                              (type ihello'replies)
                              (arguments '("void"))))))

(define ihello'reply'
  (sm:transpose-tick ihello'reply))

(define ihello'reply'reordered
  (sm:action (prefix "ihello'reply'reordered")
             (event (sm:event (constructor "ihello'Void")
                              (type ihello'replies)
                              (arguments '("void"))))))

(define ihello'end
  (sm:action (prefix "ihello'end")))

(define ihello'reorder-end
  (sm:action (prefix "ihello'reorder_end")))

(define missing-reply
  (sm:action (prefix "missing_reply")))

(define second-reply
  (sm:action (prefix "second_reply")))

(define return
  (sm:action (prefix "return")))

(define tag
  (sm:action (prefix "tag")
             (event (sm:event (type (list %sm:nat %sm:nat))))))

(define tau-void
  (sm:action (prefix "tau_void")))

(define ihello'tau-reply
  (sm:action (prefix "ihello'tau_reply")
             (event (sm:event (type ihello'replies)))))

(define ihello'behavior
  (sm:process
    (name "ihello'behavior")
    (statement (sm:goto (name "ihello'0")))))

(define ihello'0
  (sm:process
    (name "ihello'0")
    (statement (sm:goto (name "ihello'1")))))

(define ihello'1
  (sm:process
    (name "ihello'1")
    (statement
     (sm:if* (sm:literal* #t)
             (sm:goto (name "ihello'2"))))))

(define ihello'2
  (sm:process
    (name "ihello'2")
    (statement (sm:sequence*
                ihello'in
                (sm:goto (name "ihello'3"))))))

(define ihello'3
  (sm:process
    (name "ihello'3")
    (statement (sm:goto (name "ihello'4")))))

(define ihello'4
  (sm:process
    (name "ihello'4")
    (statement (sm:union*
                (sm:sequence*
                 (sm:action (prefix "tag")
                            (event (sm:event (arguments '(30 15)))))
                 (sm:goto (name "ihello'4")))
                (sm:goto (name "ihello'5"))))))

(define ihello'5
  (sm:process
    (name "ihello'5")
    (statement (sm:sequence*
                ihello'reply
                ihello'reorder-end
                (sm:goto (name "ihello'behavior"))))))

(define ihello'reorder-replied
  (let ((reply "r"))
    (sm:process
      (name "ihello'reorder_replied")
      (formals (list (sm:formal (type ihello'replies) (name reply))))
      (statement
       (sm:union*
        (sm:sequence*
         ihello'reorder-end
         (sm:invoke ihello'reply'reordered reply)
         (sm:goto (name "ihello'reorder")))
        (sm:sum
         (var "r")
         (type ihello'replies)
         (statement
          (sm:sequence*
           (sm:invoke ihello'reply var)
           second-reply
           %sm:delta))))))))

(define ihello'reorder
  (sm:process
    (name "ihello'reorder")
    (statement
     (sm:union*
      (sm:sequence*
       (sm:sum (type ihello'events)
               (statement
                (sm:sequence*
                 (sm:invoke ihello'in)
                 (sm:union*
                  (sm:sum (var "r")
                          (type ihello'replies)
                          (statement
                           (sm:sequence*
                            (sm:invoke ihello'reply var)
                            (sm:goto (name "ihello'reorder_replied")
                                     (arguments '("r"))))))
                  (sm:sequence* ihello'reorder-end missing-reply %sm:delta))))))
      (sm:sum (var "m")
              (type ihello'modeling)
              (statement
               (sm:sequence*
                (sm:invoke ihello'internal var)
                ihello'end
                (sm:goto (name "ihello'reorder")))))))))

(define ihello'reordered-parallel
  (sm:process
    (name "ihello'reordered_parallel")
    (statement (sm:parallel* "ihello'behavior" "ihello'reorder"))))

(define ihello'reordered-comm
  (sm:process
    (name "ihello'reordered_com")
    (statement
     (sm:comm
      (process ihello'reordered-parallel)
      (events (list (sm:comm-event (from (sm:multi-event (events (list ihello'in)))))
                    (sm:comm-event (from (sm:multi-event (events (list ihello'reply)))))
                    (sm:comm-event (from (sm:multi-event (events (list ihello'end)))))))))))

(define ihello'reordered-allow
  (sm:process
    (name "ihello'reordered_allow")
    (statement
     (sm:allow
      (process ihello'reordered-comm)
      (events (list ihello'reorder-end ihello'reply'reordered return tag))))))

(define ihello'reordered-rename
  (sm:process
    (name "ihello'reordered_rename")
    (statement
     (sm:rename
      (process ihello'reordered-allow)
      (events `(,(sm:rename-event (from ihello'reply') (to ihello'tau-reply))
                ,(sm:rename-event (from ihello'reply'reordered) (to ihello'reply))
                ,(sm:rename-event (from ihello'reorder-end) (to tau-void))
                ,@(sm:comm-events (sm:process-statement ihello'reordered-comm))))))))

(define ihello'interface
  (sm:process
    (name "ihello'interface")
    (statement
     (sm:hide
      (process ihello'reordered-rename)
      (events (list tau-void ihello'end ihello'tau-reply))))))

(sm:module-print-mcrl2 ihello'interface)
