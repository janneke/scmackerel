# SCMackerel --- A GNU Guile front-end for mCRL2
# Copyright © 2022, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of SCMackerel.
#
# SCMackerel is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# SCMackerel is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

dist_noinst_%C%_scm_DATA =			\
 %D%/code.scm					\
 %D%/go.scm					\
 %D%/here.scm					\
 %D%/ihello.scm					\
 %D%/main.scm					\
 %D%/stay.scm					\
 %D%/there.scm

noinst_%C%_scmdir = $(docdir)/examples
noinst_%C%_godir = $(guileobjectdir)/%D%
dist_noinst_%C%_go_DATA = $(dist_noinst_%C%_scm_DATA:%.scm=%.go)

ALL_GO += $(dist_noinst_%C%_go_DATA)
