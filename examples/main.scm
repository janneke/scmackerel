;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (srfi srfi-9 gnu)
             (scmackerel code)
             (scmackerel indent))

(define sm:comment
  (sm:comment*
   "/********************************** MAIN **************************************/\n"))

(define sm:main
  (sm:function
   (name "main")
   (statement
    (sm:compound*
     (sm:comment* "// main starts here")
     (sm:statement* "/*empty-statement*/")
     (sm:variable (type "int") (name "i"))
     (sm:variable (type "bool") (name "b") (expression "true"))
     (sm:assign* "i" (sm:not* "b"))
     (sm:compound*)
     (sm:assign* "b" (sm:not* "i"))
     (sm:compound* (sm:assign* "b" (sm:equal* "i" "b")))
     (sm:if* "b" (sm:compound*) (sm:compound* (sm:assign* "b" "b")))
     (sm:if* "b" (sm:assign* "b" "b") (sm:assign* "i" "b"))
     (sm:if* "b" (sm:compound* (sm:assign* "b" "b")))
     (sm:if* "b" (sm:compound* (sm:assign* "b" "b")) (sm:compound*))
     (sm:return* 0)))))

(define (code)
  (display (sm:code* (list sm:comment sm:main))))

((sm:indenter code))
