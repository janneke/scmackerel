;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (srfi srfi-9 gnu)
             (scmackerel code)
             (scmackerel header)
             (scmackerel indent))

(define ihello
  (let ((ihello
         (sm:struct
           (name "ihello")
           (members
            (list
             (sm:variable (type "dzn::port::meta") (name "meta")
                          (expression "m"))
             (sm:variable (type (sm:struct
                                  (members (list (sm:variable (type "std::function< void()>")
                                                              (name "hello"))))))
                          (name "in"))
             (sm:variable (type (sm:struct))
                          (name "out")))))))
    (set-field ihello
               (sm:struct-methods)
               (cons* (sm:constructor (struct ihello)
                                      (formals (list (sm:formal (type "dzn::port::meta const&")
                                                                (name "m")))))
                      (sm:destructor (struct ihello)
                                     (type "virtual")
                                     (statement "= default;"))
                      (sm:method
                       (struct ihello) (type "void") (name "dzn_check_bindings")
                       (statement
                        (sm:compound*
                         (sm:if* (sm:not* "in.hello")
                                 (sm:statement*
                                  "throw dzn::binding_error (meta, \"in.hello\")")))))
                      (sm:method (struct ihello) (type "void") (name "h_ihello"))
                      (sm:struct-methods ihello)))))

(define connect
  (sm:function (name "connect")
               (type "void")
               (formals (list (sm:formal (type "ihello&") (name "provided"))
                              (sm:formal (type "ihello&") (name "required"))))
               (statement (sm:compound*
                           (sm:assign* "provided.out" "required.out")
                           (sm:assign* "required.in" "provided.in")
                           (sm:assign* "provided.meta.require" "required.meta.require")
                           (sm:assign* "required.meta.provide" "provided.meta.provide")))))

(define hello
  (let ((hello
         (sm:struct
           (name "hello")
           (parents '("public dzn::component"))
           (members
            (list
             (sm:variable (type "dzn::meta") (name "dzn_meta")
                          (expression "{\"\",\"hello\",0,{},{},{[this]{h.dzn_check_bindings ();}}}"))
             (sm:variable (type "dzn::runtime&") (name "dzn_rt")
                          (expression "dzn_locator.get<dzn::runtime> ()"))
             (sm:variable (type "dzn::locator const&") (name "dzn_locator")
                          (expression "dzn_locator"))
             (sm:variable (type "std::function<void ()>") (name "out_h"))
             (sm:variable (type "::ihello") (name "h")
                          (expression "{{\"h\",&h,this,&dzn_meta},{\"\",0,0,0}}")))))))
    (set-field hello
               (sm:struct-methods)
               (cons* (sm:constructor
                       (struct hello)
                       (formals (list (sm:formal (type "dzn::locator const&")
                                                 (name "locator"))))
                       (statement
                        (sm:compound*
                         (sm:assign* "dzn_meta.require" "{}")
                         (sm:assign* "dzn_rt.performs_flush (this)" "true")
                         (sm:assign*
                          "h.in.hello"
                          (sm:function
                           (captures '("&"))
                           (statement
                            (sm:compound*
                             (sm:return*
                              (sm:call
                               (name "dzn::call_in")
                               (arguments
                                `("this"
                                  ,(sm:function
                                    (captures '("="))
                                    (statement
                                     (sm:compound*
                                      (sm:call-method (name "h_hello"))
                                      (sm:call-method
                                       (name "dzn_rt.flush")
                                       (arguments
                                        '("this"
                                          "dzn::coroutine_id (this->dzn_locator)")))
                                      (sm:if* "this->out_h"
                                              (sm:call-method (name "out_h")))
                                      (sm:assign* "this->out_h" "nullptr")
                                      (sm:return*))))
                                  "this->h"
                                  "\"hello\"")))))))))))
                      (sm:protection* "private")
                      (sm:method (struct hello) (type "void") (name "h_hello"))
                      (sm:struct-methods hello)))))

(define header-statements
  (list
   (sm:cpp-system-include* "dzn/meta.hh")
   (sm:namespace* "dzn"
                  (sm:statement* "struct locator")
                  (sm:statement* "struct runtime"))
   (sm:cpp-system-include* "iostream")
   (sm:cpp-system-include* "map")
   (sm:cpp-ifndef* "IHELLO_HH")
   (sm:cpp-define* "IHELLO_HH")
   ihello
   connect
   hello
   (sm:cpp-endif* "IHELLO_HH")))

(define source-statements
  (list
   ihello
   (sm:cpp-system-include* "dzn/locator.hh")
   (sm:cpp-system-include* "dzn/runtime.hh")
   connect
   hello))

(define (code)
  (format #t "/********************************* HEADER *************************************/")
  (newline)
  (display (sm:header* header-statements))
  (newline)

  (format #t "/********************************** CODE **************************************/")
  (newline)
  (display (sm:code* source-statements)))

((sm:indenter code))
