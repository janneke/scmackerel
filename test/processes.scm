;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2020,2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests processes)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 rdelim)
  #:use-module (build-aux automake)
  #:use-module (scmackerel processes))

(define (test-module)
  (let ((module (make-fresh-user-module)))
    (module-use! module (resolve-interface '(scmackerel records)))
    module))

(define (file->mcrl2 file-name)
  (catch #t
    (lambda _
      (with-output-to-string
        (lambda _ (eval-string (with-input-from-file file-name read-string) (test-module)))))
    (const #f)))

(test-begin "processes")

(test-assert "dummy"
  #t)

(test-equal "stay"
  "proc stay = delta;
init stay;
"
  (file->mcrl2 "examples/stay.scm"))

(test-equal "here"
  "act go;
proc here = go . here;
init here;
"
  (file->mcrl2 "examples/here.scm"))

(test-equal "there"
  "act back,go;
proc here = go . there;
proc there = back . here;
init here;
"
  (file->mcrl2 "examples/there.scm"))

(test-equal "go"
  "sort direction = struct there
 | back;
act go:direction;
proc here = go (there) . there;
proc there = go (back) . here;
init here;
"
  (file->mcrl2 "examples/go.scm"))

(test-equal "ihello"
  "sort Void = struct void;
sort ihello'actions = struct ihello'in'hello;
sort ihello'events = struct ihello'action (value: ihello'actions)
 | ihello'reply (value: ihello'replies);
sort ihello'modeling = struct ihello'optional
 | ihello'inevitable;
sort ihello'replies = struct ihello'nil ? ihello'nil_is
 | ihello'Void (value: Void);
act ihello'end,ihello'end',ihello'reorder_end,missing_reply,return,second_reply,tag,tau_void;
act ihello'in:ihello'events;
act ihello'in':ihello'events;
act ihello'internal:ihello'modeling;
act ihello'reply:ihello'replies;
act ihello'reply':ihello'replies;
act ihello'reply'reordered:ihello'replies;
act ihello'tau_reply:ihello'replies;
act tag:Nat # Nat;
proc ihello'0 = ihello'1;
proc ihello'1 = ((true) -> ihello'2);
proc ihello'2 = ihello'in (ihello'action (ihello'in'hello)) . ihello'3;
proc ihello'3 = ihello'4;
proc ihello'4
 = tag (30, 15) . ihello'4
 + ihello'5;
proc ihello'5 = ihello'reply (ihello'Void (void)) . ihello'reorder_end . ihello'behavior;
proc ihello'behavior = ihello'0;
proc ihello'interface = hide ({
   tau_void
 , ihello'end
 , ihello'tau_reply
 }, ihello'reordered_rename);
proc ihello'reorder
 = sum i: ihello'events . ihello'in (i) . (sum r: ihello'replies . ihello'reply (r) . ihello'reorder_replied (r)
 + ihello'reorder_end . missing_reply . delta)
 + sum m: ihello'modeling . ihello'internal (m) . ihello'end . ihello'reorder;
proc ihello'reorder_replied (r: ihello'replies)
 = ihello'reorder_end . ihello'reply'reordered (r) . ihello'reorder
 + sum r: ihello'replies . ihello'reply (r) . second_reply . delta;
proc ihello'reordered_allow = allow ({
   ihello'in'
 , ihello'reply'
 , ihello'end'
 , ihello'reorder_end
 , ihello'reply'reordered
 , return
 , tag
 }, ihello'reordered_com);
proc ihello'reordered_com = comm ({
   ihello'in | ihello'in -> ihello'in'
 , ihello'reply | ihello'reply -> ihello'reply'
 , ihello'end | ihello'end -> ihello'end'
 }, ihello'reordered_parallel);
proc ihello'reordered_parallel = ihello'behavior || ihello'reorder;
proc ihello'reordered_rename = rename ({
   ihello'reply' -> ihello'tau_reply
 , ihello'reply'reordered -> ihello'reply
 , ihello'reorder_end -> tau_void
 , ihello'in' -> ihello'in
 , ihello'end' -> ihello'end
 }, ihello'reordered_allow);
init ihello'interface;
"
  (file->mcrl2 "examples/ihello.scm"))

(test-end)
