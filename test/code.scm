;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2020, 2022, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests code)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 rdelim)
  #:use-module (build-aux automake)
  #:use-module (scmackerel code)
  #:use-module (scmackerel header))

(define (test-module)
  (let ((module (make-fresh-user-module)))
    (module-use! module (resolve-interface '(scmackerel records)))
    module))

(define (file->c++ file-name)
  (catch #t
    (lambda _
      (with-output-to-string
        (lambda _ (eval-string (with-input-from-file file-name read-string) (test-module)))))
    (const #f)))

(test-begin "code")

(test-assert "dummy"
  #t)

(test-equal "main"
  "/********************************** MAIN **************************************/
int
main ()
{
  // main starts here
  /*empty-statement*/;
  int i;
  bool b = true;
  i = !b;
    {}
  b = !i;
    {
      b = i == b;
    }
  if (b)   {}
  else
    {
      b = b;
    }
  if (b) b = b;
  else i = b;
  if (b)
    {
      b = b;
    }
  if (b)
    {
      b = b;
    }
  else   {}
  return 0;
}
"
  (file->c++ "examples/main.scm"))

(test-equal "code"
  "/********************************* HEADER *************************************/
#include <dzn/meta.hh>
namespace dzn
{
  struct locator;
  struct runtime;
}
#include <iostream>
#include <map>
#ifndef IHELLO_HH
#define IHELLO_HH
struct ihello
{
  dzn::port::meta meta;
  struct
    {
      std::function< void()> hello;
    } in;
  struct
    {
    } out;
  ihello (dzn::port::meta const& m);
  virtual ~ihello ();
  void dzn_check_bindings ();
  void h_ihello ();
};
void connect (ihello& provided, ihello& required);
struct hello: public dzn::component
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;
  std::function<void ()> out_h;
  ::ihello h;
  hello (dzn::locator const& locator);
  private:
  void h_hello ();
};
#endif // IHELLO_HH

/********************************** CODE **************************************/
ihello::ihello (dzn::port::meta const& m)
: meta (m)
{}
ihello::~ihello ()= default;
void
ihello::dzn_check_bindings ()
{
  if (!in.hello) throw dzn::binding_error (meta, \"in.hello\");
}
void
ihello::h_ihello ()
{}
#include <dzn/locator.hh>
#include <dzn/runtime.hh>
void
connect (ihello& provided, ihello& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.require = required.meta.require;
  required.meta.provide = provided.meta.provide;
}
hello::hello (dzn::locator const& locator)
: dzn_meta ({\"\",\"hello\",0,  {},  {},  {[this]  {h.dzn_check_bindings ();}}})
, dzn_rt (dzn_locator.get<dzn::runtime> ())
, dzn_locator (dzn_locator)
, h ({  {\"h\",&h,this,&dzn_meta},  {\"\",0,0,0}})
{
  dzn_meta.require =   {};
  dzn_rt.performs_flush (this) = true;
  h.in.hello = [&] ()
    {
      return dzn::call_in (this, [=] ()
        {
          this->h_hello ();
          this->dzn_rt.flush (this, dzn::coroutine_id (this->dzn_locator));
          if (this->out_h) this->out_h ();
          this->out_h = nullptr;
          return;
        }, this->h, \"hello\");
    };
}
void
hello::h_hello ()
{}
"
  (file->c++ "examples/code.scm"))

(test-end)
