;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel expressions)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel util)
  #:export (sm:and*
            sm:conditional*
            sm:equal*
            sm:greater*
            sm:greater-equal*
            sm:in*
            sm:is*
            sm:less*
            sm:less-equal*
            sm:minus*
            sm:not-equal*
            sm:or*
            sm:plus*
            sm:<
            sm:<=
            sm:==
            sm:!=
            sm:>
            sm:>=
            sm:-
            sm:+

            <sm:expresion>
            sm:expression
            sm:expression*
            sm:expression?
            sm:expression-operator
            sm:expression-operands
            sm:expression->string
            <sm:group>
            sm:group
            sm:group*
            sm:group?
            sm:group-expression
            <sm:literal>
            sm:literal
            sm:literal*
            sm:literal?
            sm:literal-expression
            <sm:not>
            sm:not
            sm:not*
            sm:not?
            sm:not
            sm:not-expression?
            sm:not-expression
            sm:print-expressions
            <sm:set>
            sm:set
            sm:set*
            sm:set?
            sm:set-elements
            <sm:var>
            sm:var
            sm:var*
            sm:member*
            sm:var?
            sm:var-name
            sm:var-member?))

;;; Commentary:
;;;
;;; This module provides expressions.
;;;
;;; Code:

;;;
;;; Expression.
;;;
(define-record-type* <sm:expresion>
  sm:expression make-sm:expression
  sm:expression?
  (operator sm:expression-operator)
  (operands sm:expression-operands
            (default '())))

(define-syntax sm:expression*
  (lambda (x)
    "Return an instance of <sm:expresion>."
    (syntax-case x ()
      ((_ operator)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg operator))
             (if (atom? arg) (make-sm:expression arg '())
                 (make-sm:expression (car arg) (cdr arg))))))
      ((_ operator operand)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg operand))
             (if (atom? arg) (make-sm:expression operator (list arg))
                 (make-sm:expression operator arg)))))
      ((_ operator operand ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list operand ...)))
             (make-sm:expression operator arg)))))))

(define-syntax sm:and*
  (syntax-rules ()
    "Return an instance of <and>."
    ((_ operand ...)
     (sm:expression* "&&" operand ...))))

(define-syntax sm:conditional*
  (syntax-rules ()
    "Return an instance of <sm:expresion>."
    ((_ condition then else)
     (make-sm:expression "?" (list condition then else)))))

(define-syntax sm:?
  (syntax-rules ()
    "Return an instance of <sm:expresion>."
    ((_ condition then else)
     (make-sm:expression "?" (list condition then else)))))

(define-syntax sm:conditional*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ condition then else)
     (make-sm:expression "?" (list condition then else)))))

(define-syntax sm:equal*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "==" operand ...))))

(define-syntax sm:==
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "==" operand ...))))

(define-syntax sm:greater*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* ">" operand ...))))

(define-syntax sm:>
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* ">" operand ...))))

(define-syntax sm:greater-equal*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* ">=" operand ...))))

(define-syntax sm:>=
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* ">=" operand ...))))

(define-syntax sm:in*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "in" operand ...))))

(define-syntax sm:is*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "=" operand ...))))

(define-syntax sm:=
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "=" operand ...))))

(define-syntax sm:less*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "<" operand ...))))

(define-syntax sm:<
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "<" operand ...))))

(define-syntax sm:less-equal*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "<=" operand ...))))

(define-syntax sm:<=
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "<=" operand ...))))

(define-syntax sm:minus*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "-" operand ...))))

(define-syntax sm:-
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "-" operand ...))))

(define-syntax sm:not-equal*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "!=" operand ...))))

(define-syntax sm:!=
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "!=" operand ...))))

(define-syntax sm:or*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "||" operand ...))))

(define-syntax sm:plus*
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "+" operand ...))))

(define-syntax sm:+
  (syntax-rules ()
    "Return an instance <sm:expression>."
    ((_ operand ...)
     (sm:expression* "+" operand ...))))

(define (sm:binary-expression? o)
  (match o
    (($ <sm:expresion> operator) (sm:binary-expression? operator))
    ((or "&&"
         "?"                            ; ternary needs parens too
         "=="
         ">="
         ">"
         "in"
         "is"
         "<"
         "<="
         "-"
         "!="
         "||"
         "+") #t)
    (_ #f)))

(define (print-sm:expression expression port)
  "Write a concise representation of EXPRESSION to PORT."
  (set-record-printers!)
  (match expression
    (($ <sm:expresion> "?" (condition then else))
     (display condition port)
     (display " ? " port)
     (display then port)
     (display " : " port)
     (display else port))
    (($ <sm:expresion> operator (operands ...))
     (let ((operator (if (equal? operator "=") operator
                         (string-append " " operator " "))))
       (display-join* operands
                      #:port port
                      #:grammar (list operator)
                      #:display-element
                      (match-lambda*
                        (((and expression
                               ($ <sm:expresion>
                                  (or "and" "or")
                                  (left right rest ...)))
                          port)
                         (display "(" port)
                         (display expression port)
                         (display ")" port))
                        ((expression port)
                         (display expression port))))))))


;;;
;;; Group.
;;;
(define-record-type* <sm:group>
  sm:group make-sm:group
  sm:group?
  (expression sm:group-expression))

(define-syntax sm:group*
  (syntax-rules ()
    "Return an instance of <sm:group>."
    ((_ expression)
     (make-sm:group expression))))

(define (print-group group port)
  "Write a concise representation of GROUP to PORT."
  (match group
    (($ <sm:group> expression)
     (display "(" port)
     (display expression port)
     (display ")" port))))


;;;
;;; Literal.
;;;
(define-record-type* <sm:literal>
  sm:literal make-sm:literal
  sm:literal?
  (value sm:literal-value))

(define-syntax sm:literal*
  (syntax-rules ()
    "Return an instance of <sm:literal>."
    ((_ expression)
     (make-sm:literal expression))))

(define (print-sm:literal literal port)
  "Write a concise representation of LITERAL to PORT."
  (match literal
    (($ <sm:literal> #t) (display "true" port))
    (($ <sm:literal> #f) (display "false" port))
    (($ <sm:literal> value) (display value port))))


;;;
;;; Not-expression.
;;;
(define-record-type* <sm:not>
  sm:not make-sm:not
  sm:not?
  (expression sm:not-expression))

(define-syntax sm:not*
  (syntax-rules ()
    "Return an instance of <sm:not>."
    ((_ expression)
     (make-sm:not expression))))

(define (print-sm:not sm:not port)
  "Write a concise representation of NOT* to PORT."
  (match sm:not
    (($ <sm:not> (and expression (? sm:binary-expression?)))
     (display "!(" port)
     (display expression port)
     (display ")" port))
    (($ <sm:not> expression)
     (display "!" port)
     (display expression port))))


;;;
;;; Var.
;;;
(define-record-type* <sm:var>
  sm:var   make-sm:var
  sm:var?
  (name    sm:var-name)
  (member? sm:var-member?
           (default #f)))

(define-syntax sm:var*
  (syntax-rules ()
    "Return an instance of <sm:var>."
    ((_ name)
     (make-sm:var name))
    ((_ member name)
     (make-sm:var name member))))

(define-syntax sm:member*
  (syntax-rules ()
    "Return an instance of <sm:var>."
    ((_ name)
     (make-sm:var name "this->"))
    ((_ member name)
     (make-sm:var name member))))

(define (print-var var port)
  "Write a concise representation of VAR to PORT."
  (match var
    (($ <sm:var> name #f)
     (display name port))
    (($ <sm:var> name #t)
     (display name port))
    (($ <sm:var> name member?)
     (display member? port)
     (display name port))))


;;;
;;; Set.
;;;
(define-record-type* <sm:set>
  sm:set make-sm:set
  sm:set?
  (elements sm:set-elements))

(define-syntax sm:set*
  (lambda (x)
    "Return an instance of <sm:set>."
    (syntax-case x ()
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:set (list arg))
                 (make-sm:set arg)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:set arg)))))))

(define (print-set set port)
  "Write a concise representation of SET to PORT."
  (match set
    (($ <sm:set> elements)
     (display "{" port)
     (display-join elements port ", ")
     (display "}" port))))

(define (set-record-printers!)
  (set-record-type-printer! <sm:expresion> print-sm:expression)
  (set-record-type-printer! <sm:group> print-group)
  (set-record-type-printer! <sm:literal> print-sm:literal)
  (set-record-type-printer! <sm:not> print-sm:not)
  (set-record-type-printer! <sm:set> print-set)
  (set-record-type-printer! <sm:var> print-var))

(define (reset-record-printers!)
  (set-record-type-printer! <sm:expresion> #f)
  (set-record-type-printer! <sm:group> #f)
  (set-record-type-printer! <sm:literal> #f)
  (set-record-type-printer! <sm:not> #f)
  (set-record-type-printer! <sm:set> #f)
  (set-record-type-printer! <sm:var> #f))

(set-record-printers!)

(define* (sm:print-expressions expression #:optional (port (current-output-port)))
  (set-record-printers!)
  (catch #t
    (lambda _ (display expression port))
    (lambda (key . args)
      (reset-record-printers!)
      (apply throw key args))))

(define (sm:expression->string expression)
  (with-output-to-string
    (cute sm:print-expressions expression)))
