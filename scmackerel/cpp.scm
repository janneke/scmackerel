;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel cpp)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel common)
  #:use-module (scmackerel util)
  #:export (<sm:cpp>
            sm:cpp
            sm:cpp?
            sm:cpp-command
            sm:cpp-argument
            sm:cpp*
            sm:cpp-define*
            sm:cpp-if*
            sm:cpp-ifndef*
            sm:cpp-endif*
            sm:cpp-include*
            sm:cpp-system-include*))

;;; Commentary:
;;;
;;; This module provides a high-level mechanism to define processes.
;;;
;;; Try: C-c C-a (sm:process (name "stay") (statement "delta")) RET
;;;
;;; Code:

;;;
;;; Preprocessor.
;;;
(define-record-type* <sm:cpp>
  sm:cpp make-sm:cpp
  sm:cpp?
  (command  sm:cpp-command)
  (argument sm:cpp-argument))

(define-syntax sm:cpp*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ command argument)
     (make-sm:cpp command argument))))

(define-syntax sm:cpp-define*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ name)
     (make-sm:cpp "define" name))
    ((_ name value)
     (make-sm:cpp "define" (simple-format #f "~a ~s" name value)))))

(define-syntax sm:cpp-if*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ name)
     (make-sm:cpp "if" name))))

(define-syntax sm:cpp-ifndef*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ name)
     (make-sm:cpp "ifndef" name))))

(define-syntax sm:cpp-endif*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ name)
     (make-sm:cpp "endif //" name))))

(define-syntax sm:cpp-include*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ file-name)
     (make-sm:cpp "include" (simple-format #f "~s" file-name)))))

(define-syntax sm:cpp-system-include*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ file-name)
     (make-sm:cpp "include" (simple-format #f "<~a>" file-name)))))

(define (print-sm:cpp cpp port)
  "Write a concise representation of CPP to PORT."
  (match cpp
    (($ <sm:cpp> command argument)
     (simple-format port "#~a ~a\n" command argument))))

(set-record-type-printer! <sm:cpp> print-sm:cpp)
