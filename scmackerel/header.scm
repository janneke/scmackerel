;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2023 Paul Hoogendijk <paul@dezyne.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel header)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel common)
  #:use-module (scmackerel expressions)
  #:use-module (scmackerel code)
  #:use-module (scmackerel util)
  #:export (sm:header
             sm:header*
             sm:header?
             sm:header-statements))

;;; Commentary:
;;;
;;; This module handles printing a header for a C++ AST.
;;;
;;; Code:

;;;
;;; Unexported reuse.
;;;
(define <sm:call> (@@ (scmackerel code) <sm:call>))
(define <sm:call-method> (@@ (scmackerel code) <sm:call-method>))
(define <sm:compound> (@@ (scmackerel code) <sm:compound>))
(define <sm:constructor> (@@ (scmackerel code) <sm:constructor>))
(define <sm:destructor> (@@ (scmackerel code) <sm:destructor>))
(define <sm:enum> (@@ (scmackerel code) <sm:enum>))
(define <sm:enum-struct> (@@ (scmackerel code) <sm:enum-struct>))
(define <sm:function> (@@ (scmackerel code) <sm:function>))
(define <sm:member-init> (@@ (scmackerel code) <sm:member-init>))
(define <sm:method> (@@ (scmackerel code) <sm:method>))
(define <sm:namespace> (@@ (scmackerel code) <sm:namespace>))
(define <sm:protection> (@@ (scmackerel code) <sm:protection>))
(define <sm:return> (@@ (scmackerel code) <sm:return>))
(define <sm:code> (@@ (scmackerel code) <sm:code>))
(define <sm:struct> (@@ (scmackerel code) <sm:struct>))
(define <sm:typedef> (@@ (scmackerel code) <sm:typedef>))
(define <sm:variable> (@@ (scmackerel code) <sm:variable>))

(define print-sm:formal (@@ (scmackerel common) print-sm:formal))
(define print-sm:namespace (@@ (scmackerel code) print-sm:namespace))
(define print-sm:typedef (@@ (scmackerel code) print-sm:typedef))

(define code:set-record-printers! (@@ (scmackerel code) set-record-printers!))
(define code:reset-record-printers! (@@ (scmackerel code) reset-record-printers!))

;;;
;;; Helpers.
;;;
(define display-top
  (match-lambda*
    (((and (or ($ <sm:enum>)
               ($ <sm:enum-struct>)
               ($ <sm:struct>)
               ($ <sm:variable>))
           statement)
      port)
     (display statement port)
     (display ";\n" port))
    ((statement port)
     (display statement port))))

(define (print-sm:namespace namespace port)
  "Write a concise representation of NAMESPACE to PORT."
  (match namespace
    (($ <sm:namespace> name statements)
     (simple-format port "namespace ~a\n{\n" name)
     (display-join* statements #:port port #:display-element display-top)
     (display "}\n" port))))


;;;
;;; Variable.
;;;
(define (print-sm:variable variable port)
  "Write a concise representation of VARIABLE to PORT."
  (match variable
    (($ <sm:variable> type name)
     (simple-format port "~a ~a" type name))))


;;;
;;; Function.
;;;
(define (print-sm:function function port)
  "Write a concise representation of FUNCTION to PORT."
  (match function
    (($ <sm:function> type name #f formals statement #t)
     (simple-format port "~a ~a (" (or type "int") name)
     (display-join formals port ", ")
     (display ")" port)
     (match statement
       (($ <sm:compound>) (display "\n" port))
       (_ #t))
     (display statement port)
     (newline port))
    (($ <sm:function> type name #f formals statement #f)
     (simple-format port "~a ~a (" (or type "int") name)
     (display-join formals port ", ")
     (display ");\n" port))
    (_
     #t)))


;;;
;;; Constructor.
;;;
(define (print-sm:constructor constructor port)
  "Write a concise representation of CONSTRUCTOR to PORT."
  (match constructor
    (($ <sm:constructor> struct type formals statement #t)
     (let ((name (sm:struct-name struct)))
       (when type
         (simple-format port "~a " type))
       (simple-format port "~a (" name)
       (display-join formals port ", ")
       (display ")\n" port)
       (let* ((inits (sm:struct-inits struct))
              (members (sm:struct-members struct))
              (init-members (filter sm:variable-expression members))
              (inits (append inits init-members))
              (init (map (lambda (m) (sm:member-init (sm:variable m))) inits)))
         (display-join init port " : " 'pre "\n , " 'infix))
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))
    (($ <sm:constructor> struct type formals statement #f)
     (let ((name (sm:struct-name struct)))
       (when type
         (simple-format port "~a " type))
       (simple-format port "~a (" name)
       (display-join formals port ", ")
       (display ");\n" port)))))


;;;
;;; Destructor.
;;;
(define (print-sm:destructor destructor port)
  "Write a concise representation of DESTRUCTOR to PORT."
  (match destructor
    (($ <sm:destructor> struct type statement #t)
     (let ((name (sm:struct-name struct)))
       (simple-format port "~~~a ()" name)
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))
    (($ <sm:destructor> struct type statement #f)
     (let ((name (sm:struct-name struct)))
       (when type
         (simple-format port "~a " type))
       (simple-format port "~~~a ();\n" name)))))


;;;
;;; Method.
;;;
(define (print-sm:method method port)
  "Write a concise representation of METHOD to PORT."
  (match method
    (($ <sm:method> struct type name formals #f)
     (simple-format port "~a ~a (" type name)
     (display-join formals port ", ")
     (display ") = 0;\n" port))
    (($ <sm:method> struct type name formals statement #t)
     (let ((struct-name (sm:struct-name struct)))
       (simple-format port "~a ~a (" type name)
       (display-join formals port ", ")
       (display ")" port)
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))
    (($ <sm:method> struct type name formals statement #f)
     (simple-format port "~a ~a (" type name)
     (display-join formals port ", ")
     (display ");\n" port))))


;;;
;;; Struct.
;;;
(define (print-sm:struct struct port)
  "Write a concise representation of STRUCT to PORT."
  (match struct
    (($ <sm:struct> name parents inits types members methods)
     (display "struct" port)
     (when name
       (simple-format port " ~a" name))
     (display-join parents port ": " 'pre ", " 'infix)
     (display "\n{\n" port)
     (display-join* types #:port port #:display-element display-top)
     (let ((members (map (lambda (m)
                           (if (not (sm:variable? m)) m
                               (set-field m (sm:variable-expression) #f)))
                         members)))
       (display-join* members #:port port #:display-element display-top))
     (display-join methods port)
     (display "}" port))))


;;;
;;; Protection.
;;;
(define (print-sm:protection protection port)
  "Write a concise representation of PROTECTION to PORT."
  (match protection
    (($ <sm:protection> statement)
     (display statement port)
     (display ":\n" port))))


;;;
;;; Enum.
;;;
(define (print-sm:enum enum port)
  "Write a concise representation of ENUM to PORT."
  (match enum
    (($ <sm:enum> #f fields)
     (display "enum \n{\n" port)
     (display-join fields port ",")
     (display "\n}" port))
    (($ <sm:enum> name fields)
     (simple-format port "enum ~a\n{\n" name)
     (display-join fields port ",")
     (display "\n}" port))))


;;;
;;; Enum-struct.
;;;
(define (print-sm:enum-struct enum-struct port)
  "Write a concise representation of ENUM-STRUCT to PORT."
  (match enum-struct
    (($ <sm:enum-struct> name fields)
     (simple-format port "enum struct ~a\n{\n" name)
     (display-join fields port ",")
     (display "\n}" port))))


;;;
;;; Scmheader.
;;;
(define-record-type* <sm:header>
  sm:header make-sm:header
  sm:header?
  (statements sm:header-statements
              (default '())))

(define-syntax sm:header*
  (syntax-rules ()
    "Return an instance of <sm:header>."
    ((_ )
     (make-sm:header '()))
    ((_ statement)
     (if (pair? statement) (make-sm:header statement)
         (make-sm:header (list statement))))
    ((_ statements ...)
     (make-sm:header (list statements ...)))))

(define (print-sm:header header port)
  "Write a concise representation of HEADER to PORT."
  (code:set-record-printers!)
  (set-record-printers!)
  (catch #t
    (lambda _
      (match header
        (($ <sm:header> statements)
         (display-join* statements #:port port #:display-element display-top))))
    (lambda (key . args)
      (code:reset-record-printers!)
      (reset-record-printers!)
      (apply throw key args))))

(set-record-type-printer! <sm:header> print-sm:header)

(define (set-record-printers!)
  (set-record-type-printer! <sm:constructor> print-sm:constructor)
  (set-record-type-printer! <sm:destructor> print-sm:destructor)
  (set-record-type-printer! <sm:enum> print-sm:enum)
  (set-record-type-printer! <sm:enum-struct> print-sm:enum-struct)
  (set-record-type-printer! <sm:function> print-sm:function)
  (set-record-type-printer! <sm:formal> print-sm:formal)
  (set-record-type-printer! <sm:method> print-sm:method)
  (set-record-type-printer! <sm:namespace> print-sm:namespace)
  (set-record-type-printer! <sm:protection> print-sm:protection)
  (set-record-type-printer! <sm:header> print-sm:header)
  (set-record-type-printer! <sm:struct> print-sm:struct)
  (set-record-type-printer! <sm:typedef> print-sm:typedef)
  (set-record-type-printer! <sm:variable> print-sm:variable))

(define (reset-record-printers!)
  (set-record-type-printer! <sm:constructor> #f)
  (set-record-type-printer! <sm:destructor> #f)
  (set-record-type-printer! <sm:enum> #f)
  (set-record-type-printer! <sm:enum-struct> #f)
  (set-record-type-printer! <sm:function> #f)
  (set-record-type-printer! <sm:formal> #f)
  (set-record-type-printer! <sm:method> #f)
  (set-record-type-printer! <sm:namespace> #f)
  (set-record-type-printer! <sm:protection> #f)
  (set-record-type-printer! <sm:header> #f)
  (set-record-type-printer! <sm:struct> #f)
  (set-record-type-printer! <sm:typedef> #f)
  (set-record-type-printer! <sm:variable> #f))
