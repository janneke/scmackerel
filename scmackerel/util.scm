;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2020, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel util)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)

  #:use-module (ice-9 match)

  #:export (atom?
            conjoin
            disjoin
            display->string
            display-join
            display-join*
            get-record-type-printer
            pke
            string-join*
            tick
            untick))

;;; Commentary:
;;;
;;; Code:

(define (atom? o)
  (and (not (pair? o))
       (not (null? o))))

(define (disjoin . predicates)
  (lambda arguments
    (any (cut apply <> arguments) predicates)))

(define (conjoin . predicates)
  (lambda arguments
    (every (cut apply <> arguments) predicates)))

(define (display->string o)
  (with-output-to-string (cute display o)))

(define* (display-join* lst #:key
                        (port (current-output-port))
                        (display-element (cute display <> <>))
                        (grammar '()))
  "Like STRING-JOIN but displaying to PORT, also allowing \"PRE\" 'pre
and \"POST\" 'post in GRAMMAR."
  (define (reduce-sexp l)
    (unfold null? (compose (cute apply list <>) (cute list-head <> 2)) cddr l))

  (define (xassq key alist)
    (find (compose (cute eq? <> key) cadr) alist))

  (define (xassq-ref alist key)
    (and=> (xassq key alist) car))

  (let* ((grammar-alist (match grammar
                          (((and (? string?) str)) `((,str infix)))
                          (_ (reduce-sexp grammar))))
         (infix (xassq-ref grammar-alist 'infix))
         (suffix (xassq-ref grammar-alist 'suffix))
         (prefix (xassq-ref grammar-alist 'prefix))
         (pre (xassq-ref grammar-alist 'pre))
         (post (xassq-ref grammar-alist 'post)))
    (when (and pre (pair? lst))
      (display pre port))
    (let loop ((lst lst))
      (when (pair? lst)
        (when prefix
          (display prefix port))
        (display-element (car lst) port)
        (when suffix
          (display suffix port))
        (when (and (pair? (cdr lst)) infix)
          (display infix port))
        (loop (cdr lst))))
    (when (and post (pair? lst))
      (display post port))))

(define (display-join lst port . grammar)
  (display-join* lst #:port port #:grammar grammar))

(define (xassq key alist)
  (find (compose (cut eq? key <>) cdr) alist))

(define (xassq-ref alist key)
  (and=> (xassq key alist) car))

(define (list->alist lst)
  (unfold null? (compose (cut apply cons <>) (cut list-head <> 2)) cddr lst))

(define (pke . stuff)
  "Like peek (pk), writing to (CURRENT-ERROR-PORT)."
  (newline (current-error-port))
  (display ";;; " (current-error-port))
  (write stuff (current-error-port))
  (newline (current-error-port))
  (car (last-pair stuff)))

(define (string-join* lst . grammar)
  "Similar to string-join, adding: STRING . pre, STRING . post."
  (let* ((grammar (list->alist grammar))
         (joined (or (and=> (xassq-ref grammar 'prefix) (cut string-join lst <> 'prefix))
                     (and=> (xassq-ref grammar 'suffix) (cut string-join lst <> 'suffix))
                     (and=> (xassq-ref grammar 'infix) (cut string-join lst <> 'infix))
                     (apply string-append lst))))
    (if (string-null? joined) joined
        (string-append (or (xassq-ref grammar 'pre) "")
                       joined
                       (or (xassq-ref grammar 'post) "")))))

(define (tick o)
  (string-append o "'"))

(define (untick o)
  (substring o 0 (1- (string-length o))))

(define (get-record-type-printer type)
  "Get custom printer for TYPE."
  (struct-ref type vtable-index-printer))
