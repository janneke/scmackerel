;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel common)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel util)
  #:export (<sm:comment>
            sm:comment
            sm:comment*
            sm:comment?
            sm:comment-string
            <sm:formal>
            sm:formal
            sm:formal?
            sm:formal-name
            sm:formal-type
            sm:formal-default
            <sm:if>
            sm:if
            sm:if?
            sm:if*
            sm:if-expression
            sm:if-then
            sm:if-else
            <sm:statement>
            sm:statement
            sm:statement*
            sm:statement?
            sm:statement-statement))

;;; Commentary:
;;;
;;; This module provides a high-level mechanism to define processes.
;;;
;;; Try: C-c C-a (sm:process (name "stay") (statement "delta")) RET
;;;
;;; Code:

;;;
;;; Comment.
;;;
(define-record-type* <sm:comment>
  sm:comment make-sm:comment
  sm:comment?
  (comment sm:comment-string))

(define-syntax sm:comment*
  (syntax-rules ()
    "Return an instance of <sm:comment>."
    ((_ comment)
     (make-sm:comment comment))))

(define (print-sm:comment comment port)
  "Write a concise representation of COMMENT to PORT."
  (match comment
    (($ <sm:comment> string)
     (display string port))))

(set-record-type-printer! <sm:comment> print-sm:comment)


;;;
;;; Statement.
;;;
(define-record-type* <sm:statement>
  sm:statement make-sm:statement
  sm:statement?
  (statement sm:statement-statement))

(define-syntax sm:statement*
  (syntax-rules ()
    "Return an instance of <sm:statement>."
    ((_)
     (make-sm:statement #f))
    ((_ statement)
     (make-sm:statement statement))))

(define (print-sm:statement statement port)
  "Write a concise representation of STATEMENT to PORT."
  (match statement
    (($ <sm:statement> #f)
     #t)
    (($ <sm:statement> statement)
     (display statement port)))
  (display ";\n" port))

(set-record-type-printer! <sm:statement> print-sm:statement)


;;;
;;; Formal.
;;;
(define-record-type* <sm:formal>
  sm:formal make-sm:formal
  sm:formal?
  (type    sm:formal-type
           (default #f))
  (name    sm:formal-name)
  (default sm:formal-default
    (default #f)))

(define (print-sm:formal formal port)
  "Write a concise representation of FORMAL to PORT."
  (match formal
    (($ <sm:formal> #f name)
     (simple-format port "~a" name))
    (($ <sm:formal> type #f)
     (simple-format port "~a" type))
    (($ <sm:formal> type name #f)
     (simple-format port "~a ~a" type name))
    (($ <sm:formal> type name default)
     (simple-format port "~a ~a=~a" type name default))))

(set-record-type-printer! <sm:formal> print-sm:formal)


;;;
;;; If.
;;;
(define-record-type* <sm:if>
  sm:if make-sm:if
  sm:if?
  (expression sm:if-expression)
  (then       sm:if-then)
  (else       sm:if-else
              (default #f)))

(define-syntax sm:if*
  (syntax-rules ()
    "Return an instance of <sm:if>."
    ((_ expression then-statement)
     (make-sm:if expression then-statement #f))
    ((_ expression then-statement else-statement)
     (make-sm:if expression then-statement else-statement))))

(define (print-sm:if sm:if port)
  "Write a concise representation of IF* to PORT."
  (match sm:if
    (($ <sm:if> expression then #f)
     (display "if (" port)
     (display expression port)
     (simple-format port ")\n ~a" then))
    (($ <sm:if> expression then else)
     (display "if (" port)
     (display expression port)
     (simple-format port ")\n  ~a\nelse ~a" then else))))

(set-record-type-printer! <sm:if> print-sm:if)
