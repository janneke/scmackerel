;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel code)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel common)
  #:use-module (scmackerel expressions)
  #:use-module (scmackerel cpp)
  #:use-module (scmackerel util)
  #:export (sm:assign
            sm:assign*
            sm:assign?
            sm:assign-variable
            sm:assign-expression
            sm:break
            sm:break
            sm:break?
            sm:call
            sm:call-function
            sm:call-arguments
            sm:call-method
            sm:call-method?
            sm:call-method-method
            sm:call-method-arguments
            sm:code->string
            sm:compound
            sm:compound*
            sm:compound?
            sm:compound-statements
            sm:continue
            sm:continue?
            sm:statements
            sm:statements*
            sm:statements?
            sm:statements-statements
            sm:constructor
            sm:constructor?
            sm:constructor-struct
            sm:constructor-type
            sm:constructor-formals
            sm:constructor-statement
            sm:constructor-inline?
            sm:destructor
            sm:destructor?
            sm:destructor-struct
            sm:destructor-type
            sm:destructor-formals
            sm:destructor-statement
            sm:destructor-inline?
            sm:enum
            sm:enum?
            sm:enum-name
            sm:enum-fields
            sm:enum-struct
            sm:enum-struct?
            sm:enum-struct-name
            sm:enum-struct-fields
            sm:expression-statement?
            sm:function
            sm:function?
            sm:function-name
            sm:function-type
            sm:function-captures
            sm:function-formals
            sm:function-statement
            sm:function-inline?
            sm:generalized-initializer-list
            sm:generalized-initializer-list*
            sm:generalized-initializer-list?
            sm:generalized-initializer-list-initializers
            sm:generalized-initializer-list-newline?
            sm:member-init
            sm:member-init?
            sm:member-init-variable
            sm:method
            sm:method?
            sm:method-type
            sm:method-name
            sm:method-struct
            sm:method-formals
            sm:method-statement
            sm:method-inline?
            sm:namespace
            sm:namespace*
            sm:namespace?
            sm:namespace-name
            sm:namespace-statements
            sm:parent-init
            sm:parent-init-name
            sm:parent-init-arguments
            sm:parent-init?
            sm:print-code
            sm:protection
            sm:protection*
            sm:protection?
            sm:protection-statement
            sm:return
            sm:return*
            sm:return?
            sm:return-expression
            sm:code
            sm:code*
            sm:code?
            sm:code-types
            sm:code-variables
            sm:code-functions
            sm:if*
            sm:struct
            sm:struct?
            sm:struct-name
            sm:struct-parents
            sm:struct-inits
            sm:struct-types
            sm:struct-members
            sm:struct-methods
            sm:struct-partial?
            sm:switch
            sm:switch?
            sm:switch-expression
            sm:switch-cases
            sm:switch-case
            sm:switch-case?
            sm:switch-case-expression
            sm:switch-case-statement
            sm:typedef
            sm:typedef*
            sm:typedef?
            sm:typedef-type
            sm:typedef-alias
            sm:variable
            sm:variable?
            sm:variable-type
            sm:variable-name
            sm:variable-expression
            sm:while
            sm:while:statement?
            sm:while*
            sm:while-expression
            sm:while-statement)
  #:re-export (sm:and*
               sm:conditional*
               sm:equal*
               sm:greater*
               sm:greater-equal*
               sm:is*
               sm:less*
               sm:less-equal*
               sm:minus*
               sm:not-equal*
               sm:or*
               sm:plus*

               sm:comment
               sm:comment*
               sm:comment?
               sm:comment-string
               sm:cpp
               sm:cpp?
               sm:cpp-command
               sm:cpp-argument
               sm:cpp*
               sm:cpp-define*
               sm:cpp-if*
               sm:cpp-ifndef*
               sm:cpp-endif*
               sm:cpp-include*
               sm:cpp-system-include*
               sm:expression
               sm:expression*
               sm:expression?
               sm:expression-operator
               sm:expression-operands
               sm:expression->string
               sm:expression->string
               sm:formal
               sm:formal?
               sm:formal-name
               sm:formal-type
               sm:group
               sm:group*
               sm:group?
               sm:group-expression
               sm:if
               sm:if?
               sm:if-else
               sm:if-expression
               sm:if-then
               sm:not*
               sm:not-expression?
               sm:not-expression
               sm:literal
               sm:literal*
               sm:literal?
               sm:literal-expression
               sm:print-expressions
               sm:statement
               sm:statement*
               sm:statement?
               sm:statement-statement
               sm:var
               sm:var*
               sm:member*
               sm:var?
               sm:var-name
               sm:var-member?))

;;; Commentary:
;;;
;;; This module provides basic elements for a C++ source code AST.
;;;
;;; Code:

;;;
;;; Imports.
;;;

(define make-sm:if (@@ (scmackerel common) make-sm:if))


;;;
;;; Helpers.
;;;
(define (sm:expression-statement? o)
  (match o
    ((or ($ <sm:assign>) ($ <sm:call>) ($ <sm:call-method>) ($ <sm:variable>)) #t)
    (_ #f)))


;;;
;;; Namespace.
;;;
(define-record-type* <sm:namespace>
  sm:namespace make-sm:namespace
  sm:namespace?
  (name       sm:namespace-name)
  (statements sm:namespace-statements
              (default '())))

(define-syntax sm:namespace*
  (lambda (x)
    "Return an instance of <sm:namespace>."
    (syntax-case x ()
      ((_ name statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:namespace name (list arg))
                 (make-sm:namespace name arg)))))
      ((_ name statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:namespace name arg)))))))

(define (print-sm:namespace namespace port)
  "Write a concise representation of NAMESPACE to PORT."
  (match namespace
    (($ <sm:namespace> name statements)
     (simple-format port "namespace ~a\n{\n" name)
     (for-each (cute display <> port) statements)
     (display "}\n" port))))


;;;
;;; Typedef.
;;;
(define-record-type* <sm:typedef>
  sm:typedef make-sm:typedef
  sm:typedef?
  (type    sm:typedef-type)
  (alias   sm:typedef-alias))

(define-syntax sm:typedef*
  (syntax-rules ()
    "Return an instance of <sm:typedef>."
    ((_ type alias)
     (make-sm:typedef type alias))))

(define (print-sm:typedef typedef port)
  "Write a concise representation of TYPEDEF to PORT."
  (match typedef
    (($ <sm:typedef> type alias)
     (simple-format port "typedef ~a ~a;\n" type alias))))


;;;
;;; Variable.
;;;
(define-record-type* <sm:variable>
  sm:variable make-sm:variable
  sm:variable?
  (type       sm:variable-type)
  (name       sm:variable-name)
  (expression sm:variable-expression
              (default #f)))

(define (variable< a b)
  (string< (sm:variable-name a) (sm:variable-name b)))

(define (print-sm:variable variable port)
  "Write a concise representation of VARIABLE to PORT."
  (match variable
    (($ <sm:variable> type name #f)
     (simple-format port "~a ~a" type name))
    (($ <sm:variable> type name expression)
     (simple-format port "~a ~a = " type name)
     (display expression port))))


;;;
;;; Assign.
;;;
(define-record-type* <sm:assign>
  sm:assign make-sm:assign
  sm:assign?
  (variable   sm:assign-variable)
  (expression sm:assign-expression))

(define-syntax sm:assign*
  (syntax-rules ()
    "Return an instance of <sm:assign>."
    ((_ variable expression)
     (make-sm:assign variable expression))))

(define (print-sm:assign assign port)
  "Write a concise representation of ASSIGN to PORT."
  (match assign
    (($ <sm:assign> variable expression)
     (display (match variable
                (($ <sm:variable> type name) name)
                (name name))
              port)
     (display " = " port)
     (display (match expression
                (($ <sm:variable> type name) name)
                (expression expression))
              port))))


;;;
;;; Function.
;;;
(define-record-type* <sm:function>
  sm:function make-sm:function
  sm:function?
  (type      sm:function-type
             (default #f))
  (name      sm:function-name
             (default #f))
  (captures  sm:function-captures
             (default #f))
  (formals   sm:function-formals
             (default '()))
  (statement sm:function-statement)
  (inline?   sm:function-inline?
             (default #f)))

(define (function< a b)
  (string< (sm:function-name a) (sm:function-name b)))

(define (print-sm:function function port)
  "Write a concise representation of FUNCTION to PORT."
  (match function
    (($ <sm:function> #f #f captures formals statement)
     (display "[" port)
     (when captures
       (display-join captures port ","))
     (display "] (" port)
     (display-join formals port ", ")
     (display ")" port)
     (match statement
       (($ <sm:compound>) (display "\n" port))
       (_ #t))
     (display statement port))
    (($ <sm:function> type name #f formals statement #t)
     #t)
    (($ <sm:function> type name #f formals statement #f)
     (simple-format port "~a\n~a (" (or type "int") name)
     (display-join formals port ", ")
     (display ")" port)
     (match statement
       (($ <sm:compound>) (display "\n" port))
       (_ #t))
     (display statement port)
     (newline port))))


;;;
;;; Call.
;;;
(define-record-type* <sm:call>
  sm:call make-sm:call
  sm:call?
  (name      sm:call-name)
  (arguments sm:call-arguments
             (default '())))

(define (print-sm:call call port)
  "Write a concise representation of CALL to PORT."
  (match call
    (($ <sm:call> name arguments)
     (let ((name (match name
                   (($ <sm:function> type name) name)
                   (($ <sm:method> type name) (sm:member* name))
                   (name name))))
       (display name)
       (display " (" port)
       (display-join arguments port ", ")
       (display ")" port)))))


;;;
;;; Call-method.
;;;
(define-record-type* <sm:call-method>
  sm:call-method make-sm:call-method
  sm:call-method?
  (name      sm:call-method-name)
  (arguments sm:call-method-arguments
             (default '())))

(define (print-sm:call-method call-method port)
  "Write a concise representation of CALL-METHOD to PORT."
  (match call-method
    (($ <sm:call-method> name arguments)
     (let ((name (match name
                   (($ <sm:method> type name) name)
                   (name name))))
       (display (sm:member* name))
       (display " (" port)
       (display-join arguments port ", ")
       (display ")" port)))))


;;;
;;; Return.
;;;
(define-record-type* <sm:return>
  sm:return make-sm:return
  sm:return?
  (expression sm:return-expression
              (default #f)))

(define-syntax sm:return*
  (syntax-rules ()
    "Return an instance of <sm:return>."
    ((_ )
     (make-sm:return #f))
    ((_ expression)
     (make-sm:return expression))))

(define (print-sm:return return port)
  "Write a concise representation of RETURN to PORT."
  (match return
    (($ <sm:return> #f)
     (display "return;\n" port))
    (($ <sm:return> (and ($ <sm:statement>) statement))
     (display "return " port)
     (display statement port))
    (($ <sm:return> expression)
     (display "return " port)
     (display expression port)
     (display ";\n" port))))


;;;
;;; Parent-init.
;;;
(define-record-type* <sm:parent-init>
  sm:parent-init make-sm:parent-init
  sm:parent-init?
  (name      sm:parent-init-name)
  (arguments sm:parent-init-arguments))

(define (print-sm:parent-init parent-init port)
  "Write a concise representation of PARENT-INIT to PORT."
  (match parent-init
    (($ <sm:parent-init> name arguments)
     (display name port)
     (display " " port)
     (display "(" port)
     (display-join arguments port ", ")
     (display ")" port))))


;;;
;;; Member-Init.
;;;
(define-record-type* <sm:member-init>
  sm:member-init make-sm:member-init
  sm:member-init?
  (sm:variable sm:member-init-variable))

(define (print-sm:member-init member-init port)
  "Write a concise representation of MEMBER-INIT to PORT."
  (match member-init
    (($ <sm:member-init> variable)
     (let ((name (sm:variable-name variable))
           (expression (sm:variable-expression variable)))
       (simple-format port "~a (~a)" name expression)))))


;;;
;;; Struct.
;;;
(define-record-type* <sm:struct>
  sm:struct make-sm:struct
  sm:struct?
  (name     sm:struct-name
            (default #f))
  (parents  sm:struct-parents
            (default '()))
  (inits    sm:struct-inits
            (default '()))
  (types    sm:struct-types
            (default '()))
  (members  sm:struct-members
            (default '()))
  (methods  sm:struct-methods
            (default '()))
  (partial? sm:struct-partial?
            (default #f)))

(define (struct< a b)
  (string< (sm:struct-name a) (sm:struct-name b)))

(define (print-sm:struct struct port)
  "Write a concise representation of STRUCT to PORT."
  (match struct
    (($ <sm:struct> name parents inits types members methods)
     (display-join methods port))))


;;;
;;; Protection.
;;;
(define-record-type* <sm:protection>
  sm:protection make-sm:protection
  sm:protection?
  (sm:protection sm:protection-statement))

(define-syntax sm:protection*
  (syntax-rules ()
    "Return an instance of <sm:compound>."
    ((_ statement)
     (make-sm:protection statement))))

(define (print-sm:protection protection port)
  "Write a concise representation of PROTECTION to PORT."
  #t)


;;;
;;; Enum.
;;;
(define-record-type* <sm:enum>
  sm:enum make-sm:enum
  sm:enum?
  (name   sm:enum-name
          (default #f))
  (fields sm:enum-fields
          (default '())))

(define (print-sm:enum enum port)
  "Write a concise representation of ENUM to PORT."
  (match enum
    (($ <sm:enum> #f fields)
     (display "enum \n{\n" port)
     (display-join fields port ",")
     (display "\n}" port))
    (($ <sm:enum> name fields)
     (simple-format port "enum ~a\n{\n" name)
     (display-join fields port ",")
     (display "\n}" port))))


;;;
;;; Enum-struct.
;;;
(define-record-type* <sm:enum-struct>
  sm:enum-struct make-sm:enum-struct
  sm:enum-struct?
  (name    sm:enum-struct-name
           (default #f))
  (fields  sm:enum-struct-fields
           (default '())))

(define (print-sm:enum-struct enum-struct port)
  "Write a concise representation of ENUM-STRUCT to PORT."
  (match enum-struct
    (($ <sm:enum-struct> name fields)
     (simple-format port "enum struct ~a\n{\n" name)
     (display-join fields port ",")
     (display "\n}" port))))


;;;
;;; generalized-initializer-list.
;;;
(define-record-type* <sm:generalized-initializer-list>
  sm:generalized-initializer-list make-sm:generalized-initializer-list
  sm:generalized-initializer-list?
  (initializers sm:generalized-initializer-list-initializers
                (default '()))
  (newline?     sm:generalized-initializer-list-newline?
                (default #f)))

(define-syntax sm:generalized-initializer-list*
  (lambda (x)
    "Return an instance of <sm:generalized-initializer-list>."
    (syntax-case x ()
      ((_ )
       #'(make-sm:generalized-initializer-list '() #f))
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:generalized-initializer-list (list arg) #f)
                 (make-sm:generalized-initializer-list arg #f)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:generalized-initializer-list arg #f)))))))

(define (print-sm:generalized-initializer-list generalized-initializer-list port)
  "Write a concise representation of GENERALIZED-INITIALIZER-LIST to PORT."
  (match generalized-initializer-list
    (($ <sm:generalized-initializer-list> initializers #f)
     (display "{" port)
     (display-join initializers port ",")
     (display "}" port))
    (($ <sm:generalized-initializer-list> initializers #t)
     (display "{" port)
     (display-join initializers port "\n , ")
     (display "}" port))))


;;;
;;; Constructor.
;;;
(define-record-type* <sm:constructor>
  sm:constructor make-sm:constructor
  sm:constructor?
  (struct    sm:constructor-struct)
  (type      sm:constructor-type
             (default #f))
  (formals   sm:constructor-formals
             (default '()))
  (statement sm:constructor-statement
             (default (sm:compound*)))
  (inline?   sm:constructor-inline?
             (default #f)))

(define (print-sm:constructor constructor port)
  "Write a concise representation of CONSTRUCTOR to PORT."
  (match constructor
    (($ <sm:constructor> struct type formals statement #t)
     #t)
    (($ <sm:constructor> struct type formals statement #f)
     (let ((name (sm:struct-name struct)))
       (simple-format port "~a::~a (" name name)
       (display-join formals port ", ")
       (display ")\n" port)
       (let* ((inits (sm:struct-inits struct))
              (members (sm:struct-members struct))
              (init-members (filter sm:variable-expression members))
              (inits (append inits init-members))
              (init (map (lambda (m) (sm:member-init (sm:variable m))) inits)))
         (display-join init port " : " 'pre "\n , " 'infix))
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))))


;;;
;;; Destructor.
;;;
(define-record-type* <sm:destructor>
  sm:destructor make-sm:destructor
  sm:destructor?
  (struct    sm:destructor-struct)
  (type      sm:destructor-type
             (default #f))
  (statement sm:destructor-statement
             (default (sm:compound*)))
  (inline?   sm:destructor-inline?
             (default #f)))

(define (print-sm:destructor destructor port)
  "Write a concise representation of DESTRUCTOR to PORT."
  (match destructor
    (($ <sm:destructor> struct type statement #t)
     #t)
    (($ <sm:destructor> struct type statement #f)
     (let ((name (sm:struct-name struct)))
       (simple-format port "~a::~~~a ()" name name)
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))))


;;;
;;; Method.
;;;
(define-record-type* <sm:method>
  sm:method make-sm:method
  sm:method?
  (struct    sm:method-struct)
  (type      sm:method-type
             (default "int"))
  (name      sm:method-name)
  (formals   sm:method-formals
             (default '()))
  (statement sm:method-statement
             (default (sm:compound*)))
  (inline?   sm:method-inline?
             (default #f)))

(define (print-sm:method method port)
  "Write a concise representation of METHOD to PORT."
  (match method
    (($ <sm:method> struct type name formals #f)
     #t)
    (($ <sm:method> struct type name formals statement #t)
     #t)
    (($ <sm:method> struct type name formals statement #f)
     (let ((struct-name (sm:struct-name struct)))
       (simple-format port "~a\n~a::~a (" type struct-name name)
       (display-join formals port ", ")
       (display ")" port)
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))))


;;;
;;; Compound.
;;;
(define-record-type* <sm:compound>
  sm:compound make-sm:compound
  sm:compound?
  (statements sm:compound-statements))

(define-syntax sm:compound*
  (lambda (x)
    "Return an instance of <sm:compound>."
    (syntax-case x ()
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:compound (list arg))
                 (make-sm:compound arg)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:compound arg)))))))

(define display-statement
  (match-lambda*
    (((and (or (? sm:expression-statement?)
               ($ <sm:member-init>)
               (? string?))
           statement)
      port)
     (display statement port)
     (display ";\n" port))
    (((and (or ($ <sm:compound>)
               ($ <sm:comment>))
           statement)
      port)
     (display statement port)
     (newline port))
    ((statement port)
     (display statement port))))

(define (print-sm:compound compound port)
  "Write a concise representation of COMPOUND to PORT."
  (match compound
    (($ <sm:compound> ()) (display "{}" port))
    (($ <sm:compound> statements)
     (display "{\n" port)
     (display-join* statements #:port port #:display-element display-statement)
     (display "}" port))))


;;;
;;; Statements.
;;;
(define-record-type* <sm:statements>
  sm:statements make-sm:statements
  sm:statements?
  (statements sm:statements-statements))

(define-syntax sm:statements*
  (lambda (x)
    "Return an instance of <sm:statements>."
    (syntax-case x ()
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:statements (list arg))
                 (make-sm:statements arg)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:statements arg)))))))

(define (print-sm:statements statements port)
  "Write a concise representation of STATEMENTS to PORT."
  (match statements
    (($ <sm:statements> ()) #t)
    (($ <sm:statements> statements)
     (display-join* statements
                    #:port port
                    #:display-element display-statement))))


;;;
;;; If.
;;;
(define (wrap-if statement)
  (if (not (sm:if? statement)) statement
      (sm:compound* statement)))

(define-syntax sm:if*
  (syntax-rules ()
    "Return an instance of <sm:if>."
    ((_ expression then-statement)
     (make-sm:if expression (wrap-if then-statement) #f))
    ((_ expression then-statement else-statement)
     (make-sm:if expression (wrap-if then-statement) else-statement))))

(define (print-sm:if sm:if port)
  "Write a concise representation of IF* to PORT."
  (match sm:if
    (($ <sm:if> expression then #f)
     (display "if (" port)
     (display expression port)
     (display ")" port)
     (match then
       (($ <sm:compound> (_ ..1)) (newline port))
       (_ (display " " port)))
     (display then port)
     (match then
       ((? sm:expression-statement?) (display ";\n" port))
       (($ <sm:compound>) (newline port))
       (_ #t)))
    (($ <sm:if> expression then else)
     (display "if (" port)
     (display expression port)
     (display ")" port)
     (match then
       (($ <sm:compound> (_ ..1)) (newline port))
       (_ (display " " port)))
     (display then port)
     (match then
       ((? sm:expression-statement?) (display ";\n" port))
       (($ <sm:compound>) (newline port))
       (_ #t))
     (display "else" port)
     (match else
       (($ <sm:compound> (_ ..1)) (newline port))
       (_ (display " " port)))
     (display else port)
     (match else
       ((? sm:expression-statement?) (display ";\n" port))
       (($ <sm:compound>) (newline port))
       (_ #t)))))


;;;
;;; Switch.
;;;
(define-record-type* <sm:switch>
  sm:switch make-sm:switch
  sm:switch?
  (expression sm:switch-expression)
  (cases      sm:switch-cases
              (default '())))

(define (print-sm:switch switch port)
  "Write a concise representation of SWITCH to PORT."
  (match switch
    (($ <sm:switch> expression cases)
     (simple-format port "switch (~a)\n{\n" expression)
     (display-join cases port)
     (display "}\n" port))))


;;;
;;; Switch-case.
;;;
(define-record-type* <sm:switch-case>
  sm:switch-case make-sm:switch-case
  sm:switch-case?
  (label      sm:switch-case-label
              (default "case"))
  (expression sm:switch-case-expression
              (default #f))
  (statement  sm:switch-case-statement))

(define (print-sm:switch-case switch-case port)
  "Write a concise representation of SWITCH-CASE to PORT."
  (match switch-case
    (($ <sm:switch-case> label expression statement)
     (display label port)
     (when expression
       (display " " port)
       (display expression port))
     (display ":" port)
     (display (if (or (sm:compound? statement) (sm:statements? statement)) "\n" " ")
              port)
     (display statement port))))


;;;
;;; Break.
;;;
(define-record-type* <sm:break>
  sm:break make-sm:break
  sm:break?)

(define (print-sm:break break port)
  "Write a concise representation of BREAK to PORT."
  (display "break;\n" port))


;;;
;;; While.
;;;
(define-record-type* <sm:while>
  sm:while make-sm:while
  sm:while?
  (expression sm:while-expression)
  (statement  sm:while-statement))

(define-syntax sm:while*
  (syntax-rules ()
    "Return an instance of <sm:parallel>."
    ((_ expression statement)
     (make-sm:while expression statement))))

(define (print-sm:while while port)
  "Write a concise representation of IF* to PORT."
  (match while
    (($ <sm:while> expression statement)
     (display "while (" port)
     (display expression port)
     (display ")" port)
     (match statement
       (($ <sm:compound> (_ ..1)) (newline port))
       (_ (display " " port)))
     (display statement port)
     (match statement
       ((? sm:expression-statement?) (display ";\n" port))
       (($ <sm:compound>) (newline port))
       (_ #t)))))


;;;
;;; Continue.
;;;
(define-record-type* <sm:continue>
  sm:continue make-sm:continue
  sm:continue?)

(define (print-sm:continue continue port)
  "Write a concise representation of CONTINUE to PORT."
  (display "continue;\n" port))


;;;
;;; Scmcode.
;;;
(define-record-type* <sm:code>
  sm:code make-sm:code
  sm:code?
  (statements sm:code-statements
              (default '())))

(define-syntax sm:code*
  (lambda (x)
    "Return an instance of <sm:code>."
    (syntax-case x ()
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:code (list arg))
                 (make-sm:code arg)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:code arg)))))))

(define (print-sm:code code port)
  "Write a concise representation of SCMCODE to PORT."
  (set-record-printers!)
  (catch #t
    (lambda _
      (match code
        (($ <sm:code> statements)
         (for-each (cute display <> port) statements))))
    (lambda (key . args)
      (reset-record-printers!)
      (apply throw key args))))

(set-record-type-printer! <sm:code> print-sm:code)

(define (set-record-printers!)
  ((@@ (scmackerel expressions) set-record-printers!))
  (set-record-type-printer! <sm:assign> print-sm:assign)
  (set-record-type-printer! <sm:break> print-sm:break)
  (set-record-type-printer! <sm:call> print-sm:call)
  (set-record-type-printer! <sm:call-method> print-sm:call-method)
  (set-record-type-printer! <sm:compound> print-sm:compound)
  (set-record-type-printer! <sm:constructor> print-sm:constructor)
  (set-record-type-printer! <sm:continue> print-sm:continue)
  (set-record-type-printer! <sm:destructor> print-sm:destructor)
  (set-record-type-printer! <sm:function> print-sm:function)
  (set-record-type-printer! <sm:formal> (@@ (scmackerel common) print-sm:formal))
  (set-record-type-printer! <sm:generalized-initializer-list>
                            print-sm:generalized-initializer-list)
  (set-record-type-printer! <sm:if> print-sm:if)
  (set-record-type-printer! <sm:member-init> print-sm:member-init)
  (set-record-type-printer! <sm:method> print-sm:method)
  (set-record-type-printer! <sm:namespace> print-sm:namespace)
  (set-record-type-printer! <sm:parent-init> print-sm:parent-init)
  (set-record-type-printer! <sm:protection> print-sm:protection)
  (set-record-type-printer! <sm:return> print-sm:return)
  (set-record-type-printer! <sm:code> print-sm:code)
  (set-record-type-printer! <sm:statements> print-sm:statements)
  (set-record-type-printer! <sm:switch> print-sm:switch)
  (set-record-type-printer! <sm:switch-case> print-sm:switch-case)
  (set-record-type-printer! <sm:struct> print-sm:struct)
  (set-record-type-printer! <sm:typedef> print-sm:typedef)
  (set-record-type-printer! <sm:variable> print-sm:variable)
  (set-record-type-printer! <sm:while> print-sm:while))

(define (reset-record-printers!)
  ((@@ (scmackerel expressions) reset-record-printers!))
  (set-record-type-printer! <sm:assign> #f)
  (set-record-type-printer! <sm:break> #f)
  (set-record-type-printer! <sm:call> #f)
  (set-record-type-printer! <sm:call-method> #f)
  (set-record-type-printer! <sm:compound> #f)
  (set-record-type-printer! <sm:constructor> #f)
  (set-record-type-printer! <sm:continue> #f)
  (set-record-type-printer! <sm:destructor> #f)
  (set-record-type-printer! <sm:function> #f)
  (set-record-type-printer! <sm:generalized-initializer-list> #f)
  (set-record-type-printer! <sm:if> #f)
  (set-record-type-printer! <sm:member-init> #f)
  (set-record-type-printer! <sm:method> #f)
  (set-record-type-printer! <sm:namespace> #f)
  (set-record-type-printer! <sm:formal> #f)
  (set-record-type-printer! <sm:parent-init> #f)
  (set-record-type-printer! <sm:protection> #f)
  (set-record-type-printer! <sm:return> #f)
  (set-record-type-printer! <sm:code> #f)
  (set-record-type-printer! <sm:statements> #f)
  (set-record-type-printer! <sm:switch> #f)
  (set-record-type-printer! <sm:switch-case> #f)
  (set-record-type-printer! <sm:struct> #f)
  (set-record-type-printer! <sm:typedef> #f)
  (set-record-type-printer! <sm:variable> #f)
  (set-record-type-printer! <sm:while> #f))

(define* (sm:print-code code #:optional (port (current-output-port)))
  (set-record-printers!)
  (display code port)
  (reset-record-printers!)
  (set-record-type-printer! <sm:code> print-sm:code))

(define (sm:code->string code)
  (with-output-to-string
    (cute sm:print-code code)))
