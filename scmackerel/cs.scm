;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel cs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel cpp)
  #:use-module (scmackerel expressions)
  #:use-module (scmackerel code)
  #:use-module (scmackerel util)
  #:export (sm:code->string
            sm:cpp-sm:using*
            sm:print-code
            sm:member*
            sm:using
            sm:using?
            sm:using-variables
            sm:using-statement)
  #:re-export (sm:and*
               sm:break
               sm:break?
               sm:conditional*
               sm:equal*
               sm:continue
               sm:continue?
               sm:greater*
               sm:greater-equal*
               sm:is*
               sm:less*
               sm:less-equal*
               sm:minus*
               sm:not-equal*
               sm:or*
               sm:plus*

               sm:assign
               sm:assign*
               sm:assign-expression
               sm:assign-variable
               sm:assign?
               sm:call
               sm:call-arguments
               sm:call-function
               sm:call-method
               sm:call-method-arguments
               sm:call-method-method
               sm:comment
               sm:comment*
               sm:comment-string
               sm:comment?
               sm:compound
               sm:compound*
               sm:compound-statements
               sm:compound?
               sm:constructor
               sm:constructor-formals
               sm:constructor-statement
               sm:constructor-struct
               sm:constructor-type
               sm:constructor?
               sm:cpp
               sm:cpp*
               sm:cpp-argument
               sm:cpp-command
               sm:cpp?
               sm:destructor
               sm:destructor-formals
               sm:destructor-statement
               sm:destructor-struct
               sm:destructor-type
               sm:destructor?
               sm:enum-struct
               sm:enum-struct-fields
               sm:enum-struct-name
               sm:enum-struct?
               sm:expression
               sm:expression*
               sm:expression->string
               sm:expression->string
               sm:expression-operands
               sm:expression-operator
               sm:expression?
               sm:formal
               sm:formal-name
               sm:formal-type
               sm:formal?
               sm:function
               sm:function-captures
               sm:function-formals
               sm:function-name
               sm:function-statement
               sm:function-type
               sm:function?
               sm:generalized-initializer-list
               sm:generalized-initializer-list*
               sm:generalized-initializer-list-initializers
               sm:generalized-initializer-list-newline?
               sm:generalized-initializer-list?
               sm:group
               sm:group*
               sm:group-expression
               sm:group?
               sm:if*
               sm:if-else
               sm:if-expression
               sm:if
               sm:if?
               sm:if-then
               sm:literal
               sm:literal*
               sm:literal?
               sm:literal-expression
               sm:member-init
               sm:member-init-variable
               sm:member-init?
               sm:method
               sm:method-formals
               sm:method-name
               sm:method-statement
               sm:method-struct
               sm:method-type
               sm:method?
               sm:namespace
               sm:namespace*
               sm:namespace-name
               sm:namespace-statements
               sm:namespace?
               sm:not*
               sm:not-expression
               sm:not-expression?
               sm:print-expressions
               sm:parent-init
               sm:parent-init-name
               sm:parent-init-arguments
               sm:parent-init?
               sm:protection
               sm:protection*
               sm:protection-statement
               sm:protection?
               sm:return
               sm:return*
               sm:return-expression
               sm:return?
               sm:code
               sm:code*
               sm:code-functions
               sm:code-types
               sm:code-variables
               sm:code?
               sm:statement
               sm:statement*
               sm:statement-statement
               sm:statement?
               sm:statements
               sm:statements*
               sm:statements-statements
               sm:statements?
               sm:struct
               sm:struct-inits
               sm:struct-members
               sm:struct-methods
               sm:struct-name
               sm:struct-parents
               sm:struct-types
               sm:struct?
               sm:switch
               sm:switch-case
               sm:switch-case-expression
               sm:switch-case-statement
               sm:switch-case?
               sm:switch-cases
               sm:switch-expression
               sm:switch?
               sm:var
               sm:var*
               sm:var-member?
               sm:var-name
               sm:var?
               sm:variable
               sm:variable-expression
               sm:variable-name
               sm:variable-type
               sm:variable?
               sm:while
               sm:while*
               sm:while-expression
               sm:while-statement))

;;; Commentary:
;;;
;;; This module handles printing a header for a C++ AST.
;;;
;;; Code:

;;;
;;; Unexported reuse.
;;;
(define <sm:assign> (@@ (scmackerel code) <sm:assign>))
(define <sm:break> (@@ (scmackerel code) <sm:break>))
(define <sm:call> (@@ (scmackerel code) <sm:call>))
(define <sm:call-method> (@@ (scmackerel code) <sm:call-method>))
(define <sm:compound> (@@ (scmackerel code) <sm:compound>))
(define <sm:constructor> (@@ (scmackerel code) <sm:constructor>))
(define <sm:continue> (@@ (scmackerel code) <sm:continue>))
(define <sm:destructor> (@@ (scmackerel code) <sm:destructor>))
(define <sm:enum-struct> (@@ (scmackerel code) <sm:enum-struct>))
(define <sm:function> (@@ (scmackerel code) <sm:function>))
(define <sm:generalized-initializer-list> (@@ (scmackerel code)
                                              <sm:generalized-initializer-list>))
(define <sm:if> (@@ (scmackerel code) <sm:if>))
(define <sm:formal> (@@ (scmackerel code) <sm:formal>))
(define <sm:member-init> (@@ (scmackerel code) <sm:member-init>))
(define <sm:method> (@@ (scmackerel code) <sm:method>))
(define <sm:namespace> (@@ (scmackerel code) <sm:namespace>))
(define <sm:parent-init> (@@ (scmackerel code) <sm:parent-init>))
(define <sm:protection> (@@ (scmackerel code) <sm:protection>))
(define <sm:return> (@@ (scmackerel code) <sm:return>))
(define <sm:code> (@@ (scmackerel code) <sm:code>))
(define <sm:statements> (@@ (scmackerel code) <sm:statements>))
(define <sm:struct> (@@ (scmackerel code) <sm:struct>))
(define <sm:switch> (@@ (scmackerel code) <sm:switch>))
(define <sm:switch-case> (@@ (scmackerel code) <sm:switch-case>))
(define <sm:variable> (@@ (scmackerel code) <sm:variable>))
(define <sm:while> (@@ (scmackerel code) <sm:while>))

(define display-statement (@@ (scmackerel code) display-statement))
(define display-top (@@ (scmackerel header) display-top))

(define make-sm:cpp (@@ (scmackerel cpp) make-sm:cpp))
(define make-sm:var (@@ (scmackerel expressions) make-sm:var))

(define print-sm:assign (@@ (scmackerel code) print-sm:assign))
(define print-sm:break (@@ (scmackerel code) print-sm:break))
(define print-sm:call (@@ (scmackerel code) print-sm:call))
(define print-sm:compound (@@ (scmackerel code) print-sm:compound))
(define print-sm:continue (@@ (scmackerel code) print-sm:continue))
(define print-sm:formal (@@ (scmackerel common) print-sm:formal))
(define print-sm:if (@@ (scmackerel code) print-sm:if))
(define print-sm:namespace (@@ (scmackerel code) print-sm:namespace))
(define print-sm:parent-init (@@ (scmackerel code) print-sm:parent-init))
(define print-sm:return (@@ (scmackerel code) print-sm:return))
(define print-sm:statements (@@ (scmackerel code) print-sm:statements))
(define print-sm:switch (@@ (scmackerel code) print-sm:switch))
(define print-sm:switch-case (@@ (scmackerel code) print-sm:switch-case))
(define print-sm:variable (@@ (scmackerel code) print-sm:variable))
(define print-sm:while (@@ (scmackerel code) print-sm:while))

(define-syntax sm:member*
  (syntax-rules ()
    "Return an instance of <sm:var>."
    ((_ name)
     (make-sm:var name "this."))
    ((_ member name)
     (make-sm:var name member))))

;;;
;;; Function.
;;;
(define (print-sm:function function port)
  "Write a concise representation of FUNCTION to PORT."
  (match function
    (($ <sm:function> type #f captures formals statement)
     (display "(")
     (display-join formals port ", ")
     (display ") => " port)
     (match statement
       (($ <sm:compound>) (display "\n" port))
       (_ #t))
     (display statement port))
    (($ <sm:function> type name #f formals statement)
     (simple-format port "~a\n~a (" (or type "int") name)
     (display-join formals port ", ")
     (display ")" port)
     (match statement
       (($ <sm:compound>) (display "\n" port))
       (_ #t))
     (display statement port)
     (newline port))))


;;;
;;; Constructor.
;;;
(define (print-sm:constructor constructor port)
  "Write a concise representation of CONSTRUCTOR to PORT."
  (match constructor
    (($ <sm:constructor> struct type formals statement)
     (let* ((name (sm:struct-name struct))
            (inits (sm:struct-inits struct))
            (members (sm:struct-members struct))
            (init-members (filter sm:variable-expression members))
            (init (map (lambda (m) (sm:member-init (sm:variable m))) init-members))
            (statement (sm:compound*
                        `(,@init
                          ,@(sm:compound-statements statement)))))
       (simple-format port "public ~a (" name)
       (display-join formals port ", ")
       (display ")" port)
       (when (pair? inits)
         (newline port)
         (display-join inits port " : " 'pre "\n , " 'infix))
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))))


;;;
;;; Destructor.
;;;
(define (print-sm:destructor destructor port)
  "Write a concise representation of DESTRUCTOR to PORT."
  (match destructor
    (($ <sm:destructor> struct type statement)
     (let ((name (sm:struct-name struct)))
       (simple-format port "~~~a ()" name)
       (match statement
         (($ <sm:compound>) (display "\n" port))
         (_ #t))
       (display statement port)
       (newline port)))))


;;;
;;; Method.
;;;
(define (print-sm:method method port)
  "Write a concise representation of METHOD to PORT."
  (match method
    (($ <sm:method> struct type name formals statement)
     (simple-format port "public ~a\n~a (" type name)
     (display-join formals port ", ")
     (display ")" port)
     (match statement
       (($ <sm:compound>) (display "\n" port))
       (_ #t))
     (match statement
       (#f (display ";" port))
       (_ (display statement port)))
     (newline port))))


;;;
;;; Struct.
;;;
(define (print-sm:struct struct port)
  "Write a concise representation of STRUCT to PORT."
  (match struct
    (($ <sm:struct> name parents inits types members methods partial?)
     (display "public" port)
     (when partial?
       (display " partial" port))
     (display " class" port)
     (when name
       (simple-format port " ~a" name))
     (display-join parents port ": " 'pre ", " 'infix)
     (display "\n{\n" port)
     (display-join* types #:port port #:display-element display-top)
     (let ((members (map (lambda (m)
                           (set-field
                            m
                            (sm:variable-expression) #f))
                         members)))
       (display-join* members
                      #:port port
                      #:display-element
                      (match-lambda*
                        (((and ($ <sm:variable>) variable
                               (= sm:variable-type ($ <sm:struct>)))
                          port)
                         (display variable port))
                        (((and ($ <sm:variable>) variable
                               (= sm:variable-expression #f))
                          port)
                         (display "public " port)
                         (display variable port)
                         (display ";\n" port))
                        ((member port)
                         (display "public " port)
                         (display member port)))))
     (display-join methods port)
     (display "}" port))))


;;;
;;; Protection.
;;;
(define (print-sm:protection protection port)
  "Write a concise representation of PROTECTION to PORT."
  #t)


;;;
;;; Enum-struct.
;;;
(define (print-sm:enum-struct enum-struct port)
  "Write a concise representation of ENUM-STRUCT to PORT."
  (match enum-struct
    (($ <sm:enum-struct> name fields)
     (simple-format port "public enum ~a\n{\n" name)
     (display-join fields port ",")
     (display "\n}"))))


;;;
;;; Call-method.
;;;
(define (print-sm:call-method call-method port)
  "Write a concise representation of CALL-METHOD to PORT."
  (match call-method
    (($ <sm:call-method> name arguments)
     (let ((name (match name
                   (($ <sm:method> type name) name)
                   (name name))))
       (display (sm:member* name))
       (display " (")
       (display-join arguments port ", ")
       (display ")" port)))))


;;;
;;; Member-Init.
;;;
(define (print-sm:member-init member-init port)
  "Write a concise representation of MEMBER-INIT to PORT."
  (match member-init
    (($ <sm:member-init> variable)
     (let ((name (sm:variable-name variable))
           (expression (sm:variable-expression variable)))
       (simple-format port "~a = ~a" (sm:member* name) expression)))))


;;;
;;; generalized-initializer-list.
;;;
(define (print-sm:generalized-initializer-list generalized-initializer-list port)
  "Write a concise representation of GENERALIZED-INITIALIZER-LIST to PORT."
  (match generalized-initializer-list
    (($ <sm:generalized-initializer-list> initializers #f)
     (display "{")
     (display-join initializers port ",")
     (display "}"))
    (($ <sm:generalized-initializer-list> initializers #t)
     (display "{")
     (display-join initializers port "\n , ")
     (display "}"))))


;;;
;;; Cpp.
;;;
(define-syntax sm:cpp-sm:using*
  (syntax-rules ()
    "Return an instance of <sm:cpp>."
    ((_ file-name)
     (make-sm:cpp "using" file-name))))

(define (print-sm:cpp cpp port)
  "Write a concise representation of CPP to PORT."
  (match cpp
    (($ <sm:cpp> command argument)
     (simple-format port "~a ~a;\n" command argument))))


;;;
;;; Using.
;;;
(define-record-type* <sm:using>
  sm:using make-sm:using
  sm:using?
  (variables sm:using-variables
             (default '()))
  (statement sm:using-statement))

(define (print-sm:using using port)
  "Write a concise representation of USING to PORT."
  (match using
    (($ <sm:using> variables statement)
     (display "using (" port)
     (display-join variables port ",")
     (display ")" port)
     (match statement
       (($ <sm:compound>) (display "\n" port))
       (_ #t))
     (display statement port)
     (newline port))))


;;;
;;; SCMcode.
;;;
(define (print-sm:code code port)
  "Write a concise representation of SCMCODE to PORT."
  (set-record-printers!)
  (catch #t
    (lambda _
      (match code
        (($ <sm:code> statements)
         (display-join* statements #:port port #:display-element display-top))))
    (lambda (key . args)
      (reset-record-printers!)
      (apply throw key args))))

(set-record-type-printer! <sm:code> print-sm:code)

(define (set-record-printers!)
  ((@@ (scmackerel expressions) set-record-printers!))
  (set-record-type-printer! <sm:assign> print-sm:assign)
  (set-record-type-printer! <sm:break> print-sm:break)
  (set-record-type-printer! <sm:call> print-sm:call)
  (set-record-type-printer! <sm:call-method> print-sm:call-method)
  (set-record-type-printer! <sm:cpp> print-sm:cpp)
  (set-record-type-printer! <sm:compound> print-sm:compound)
  (set-record-type-printer! <sm:constructor> print-sm:constructor)
  (set-record-type-printer! <sm:continue> print-sm:continue)
  (set-record-type-printer! <sm:destructor> print-sm:destructor)
  (set-record-type-printer! <sm:enum-struct> print-sm:enum-struct)
  (set-record-type-printer! <sm:function> print-sm:function)
  (set-record-type-printer! <sm:formal> print-sm:formal)
  (set-record-type-printer! <sm:generalized-initializer-list>
                            print-sm:generalized-initializer-list)
  (set-record-type-printer! <sm:if> print-sm:if)
  (set-record-type-printer! <sm:member-init> print-sm:member-init)
  (set-record-type-printer! <sm:method> print-sm:method)
  (set-record-type-printer! <sm:namespace> print-sm:namespace)
  (set-record-type-printer! <sm:parent-init> print-sm:parent-init)
  (set-record-type-printer! <sm:protection> print-sm:protection)
  (set-record-type-printer! <sm:return> print-sm:return)
  (set-record-type-printer! <sm:code> print-sm:code)
  (set-record-type-printer! <sm:statements> print-sm:statements)
  (set-record-type-printer! <sm:switch> print-sm:switch)
  (set-record-type-printer! <sm:switch-case> print-sm:switch-case)
  (set-record-type-printer! <sm:struct> print-sm:struct)
  (set-record-type-printer! <sm:using> print-sm:using)
  (set-record-type-printer! <sm:variable> print-sm:variable)
  (set-record-type-printer! <sm:while> print-sm:while))

(define (reset-record-printers!)
  ((@@ (scmackerel expressions) reset-record-printers!))
  (set-record-type-printer! <sm:assign> #f)
  (set-record-type-printer! <sm:break> #f)
  (set-record-type-printer! <sm:call> #f)
  (set-record-type-printer! <sm:call-method> #f)
  (set-record-type-printer! <sm:cpp> #f)
  (set-record-type-printer! <sm:compound> #f)
  (set-record-type-printer! <sm:constructor> #f)
  (set-record-type-printer! <sm:continue> #f)
  (set-record-type-printer! <sm:destructor> #f)
  (set-record-type-printer! <sm:enum-struct> #f)
  (set-record-type-printer! <sm:formal> #f)
  (set-record-type-printer! <sm:function> #f)
  (set-record-type-printer! <sm:generalized-initializer-list> #f)
  (set-record-type-printer! <sm:if> #f)
  (set-record-type-printer! <sm:member-init> #f)
  (set-record-type-printer! <sm:method> #f)
  (set-record-type-printer! <sm:namespace> #f)
  (set-record-type-printer! <sm:parent-init> #f)
  (set-record-type-printer! <sm:protection> #f)
  (set-record-type-printer! <sm:return> #f)
  (set-record-type-printer! <sm:code> #f)
  (set-record-type-printer! <sm:statements> #f)
  (set-record-type-printer! <sm:switch> #f)
  (set-record-type-printer! <sm:switch-case> #f)
  (set-record-type-printer! <sm:struct> #f)
  (set-record-type-printer! <sm:using> #f)
  (set-record-type-printer! <sm:variable> #f)
  (set-record-type-printer! <sm:while> #f))

(define* (sm:print-code code #:optional (port (current-output-port)))
  (set-record-printers!)
  (display code port)
  (reset-record-printers!)
  (set-record-type-printer! <sm:code> print-sm:code))

(define (sm:code->string code)
  (with-output-to-string
    (cute sm:print-code code)))
