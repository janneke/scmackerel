;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel c-header)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel cpp)
  #:use-module (scmackerel expressions)
  #:use-module (scmackerel c)
  #:use-module (scmackerel header)
  #:use-module (scmackerel util)
  #:re-export (sm:header
                sm:header*
                sm:header?
                sm:header-statements))

;;; Commentary:
;;;
;;; This module handles printing a header for a C AST.
;;;
;;; Code:

;;;
;;; Unexported reuse.
;;;
(define <sm:constructor> (@@ (scmackerel code) <sm:constructor>))
(define <sm:destructor> (@@ (scmackerel code) <sm:destructor>))
(define <sm:enum> (@@ (scmackerel code) <sm:enum>))
(define <sm:formal> (@@ (scmackerel common) <sm:formal>))
(define <sm:function> (@@ (scmackerel code) <sm:function>))
(define <sm:method> (@@ (scmackerel code) <sm:method>))
(define <sm:header> (@@ (scmackerel header) <sm:header>))
(define <sm:struct> (@@ (scmackerel code) <sm:struct>))
(define <sm:typedef> (@@ (scmackerel code) <sm:typedef>))
(define <sm:variable> (@@ (scmackerel code) <sm:variable>))

(define print-sm:enum (@@ (scmackerel header) print-sm:enum))
(define print-sm:formal (@@ (scmackerel common) print-sm:formal))
(define print-sm:function (@@ (scmackerel header) print-sm:function))
(define print-sm:struct (@@ (scmackerel header) print-sm:struct))
(define print-sm:typedef (@@ (scmackerel header) print-sm:typedef))
(define print-sm:variable (@@ (scmackerel header) print-sm:variable))

(define display-top (@@ (scmackerel header) display-top))
(define code:set-record-printers! (@@ (scmackerel c) set-record-printers!))
(define code:reset-record-printers! (@@ (scmackerel c) reset-record-printers!))


;;;
;;; Constructor.
;;;
(define (print-sm:constructor constructor port)
  "Write a concise representation of CONSTRUCTOR to PORT."
  (match constructor
    (($ <sm:constructor> struct type formals statement #f)
     (let ((name (sm:struct-name struct))
           (type (or type "void"))
           (formals (sm:formals:add-self struct formals)))
       (simple-format port "~a ~a_init (" type name)
       (display-join formals port ", ")
       (display ");\n" port)))))


;;;
;;; Destructor.
;;;
(define (print-sm:destructor destructor port)
  "Write a concise representation of DESTRUCTOR to PORT."
  (match destructor
    (($ <sm:destructor> struct type statement #f)
     (let ((name (sm:struct-name struct))
           (type (or type "void")))
       (simple-format port "~a ~a_free ();\n" type name)))))


;;;
;;; Method.
;;;
(define (print-sm:method method port)
  "Write a concise representation of METHOD to PORT."
  (match method
    (($ <sm:method> struct type name formals statement #f)
     (let* ((struct-name (sm:struct-name struct))
            (formals (sm:formals:add-self struct formals)))
       (simple-format port "~a ~a_~a (" type struct-name name)
       (display-join formals port ", ")
       (display ");\n" port)))))


;;;
;;; SCMheader.
;;;
(define (print-sm:header header port)
  "Write a concise representation of SCMHEADER to PORT."
  (code:set-record-printers!)
  (set-record-printers!)
  (catch #t
    (lambda _
      (match header
        (($ <sm:header> statements)
         (display-join* statements #:port port #:display-element display-top))))
    (lambda (key . args)
      (code:reset-record-printers!)
      (reset-record-printers!)
      (apply throw key args))))

(set-record-type-printer! <sm:header> print-sm:header)

(define (set-record-printers!)
  (set-record-type-printer! <sm:constructor> print-sm:constructor)
  (set-record-type-printer! <sm:destructor> print-sm:destructor)
  (set-record-type-printer! <sm:enum> print-sm:enum)
  (set-record-type-printer! <sm:function> print-sm:function)
  (set-record-type-printer! <sm:formal> print-sm:formal)
  (set-record-type-printer! <sm:method> print-sm:method)
  (set-record-type-printer! <sm:header> print-sm:header)
  (set-record-type-printer! <sm:struct> print-sm:struct)
  (set-record-type-printer! <sm:typedef> print-sm:typedef)
  (set-record-type-printer! <sm:variable> print-sm:variable))

(define (reset-record-printers!)
  (set-record-type-printer! <sm:constructor> #f)
  (set-record-type-printer! <sm:destructor> #f)
  (set-record-type-printer! <sm:enum> #f)
  (set-record-type-printer! <sm:function> #f)
  (set-record-type-printer! <sm:formal> #f)
  (set-record-type-printer! <sm:method> #f)
  (set-record-type-printer! <sm:header> #f)
  (set-record-type-printer! <sm:struct> #f)
  (set-record-type-printer! <sm:typedef> #f)
  (set-record-type-printer! <sm:variable> #f))
