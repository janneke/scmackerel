;;; SCMackerel --- A GNU Guile front-end for mCRL2
;;; Copyright © 2020, 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2023 Paul Hoogendijk <paul@dezyne.org>
;;;
;;; This file is part of SCMackerel.
;;;
;;; SCMackerel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; SCMackerel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (scmackerel processes)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (scmackerel records)
  #:use-module (scmackerel common)
  #:use-module (scmackerel expressions)
  #:use-module (scmackerel util)
  #:export (%sm:bool
            %sm:delta
            %sm:int
            %sm:nat
            sm:action
            sm:action?
            sm:action-prefix
            sm:action-event
            sm:allow
            sm:allow?
            sm:allow-events
            sm:allow-process
            sm:block
            sm:block?
            sm:block-events
            sm:block-process
            sm:comm
            sm:comm?
            sm:comm-events
            sm:comm-process
            sm:comm-event
            sm:comm-event?
            sm:comm-event-from
            sm:comm-event-to
            sm:construct
            sm:entity
            sm:entity?
            sm:entity-name
            sm:entity-formals
            sm:equation
            sm:equation?
            sm:equation-left
            sm:equation-right
            sm:event
            sm:event?
            sm:event-type
            sm:event-constructor
            sm:event-arguments
            sm:goto
            sm:goto?
            sm:goto-arguments
            sm:goto-name
            sm:hide
            sm:hide?
            sm:hide-events
            sm:hide-process
            sm:invoke
            sm:module->mcrl2
            sm:module-print-mcrl2
            sm:multi-event
            sm:multi-event?
            sm:multi-event-events
            sm:parallel
            sm:parallel*
            sm:parallel?
            sm:parallel-processes
            sm:predicate
            sm:predicate?
            sm:predicate-type
            sm:predicate-name
            sm:print-mcrl2
            sm:process-actions
            sm:process
            sm:process?
            sm:process-formals
            sm:process-name
            sm:process-statement
            sm:processes->string
            sm:rename
            sm:rename?
            sm:rename-events
            sm:rename-process
            sm:rename-event
            sm:rename-event?
            sm:rename-event-from
            sm:rename-event-to
            sm:mcrl2
            sm:mcrl2?
            sm:mcrl2-types
            sm:mcrl2-equations
            sm:mcrl2-actions
            sm:mcrl2-processes
            sm:mcrl2-init
            sm:sequence
            sm:sequence*
            sm:sequence?
            sm:sequence-statements
            sm:sum
            sm:sum?
            sm:sum-var
            sm:sum-type
            sm:sum-statement
            sm:transpose-tick
            sm:type
            sm:type?
            sm:type-name
            sm:type-entities
            sm:union
            sm:union*
            sm:union?
            sm:union-statements)
  #:re-export (sm:and*
               sm:equal*
               sm:greater*
               sm:greater-equal*
               sm:in*
               sm:is*
               sm:less*
               sm:less-equal*
               sm:minus*
               sm:not-equal*
               sm:or*
               sm:plus*
               sm:<
               sm:<=
               sm:==
               sm:!=
               sm:>
               sm:>=
               sm:-
               sm:+

               sm:comment
               sm:comment*
               sm:comment?
               sm:comment-string
               sm:expression
               sm:expression*
               sm:expression?
               sm:expression-operator
               sm:expression-operands
               sm:expression->string
               sm:formal
               sm:formal?
               sm:formal-name
               sm:formal-type
               sm:group
               sm:group*
               sm:group?
               sm:group-expression
               sm:if
               sm:if?
               sm:if*
               sm:if-else
               sm:if-expression
               sm:if-then
               sm:literal
               sm:literal*
               sm:literal?
               sm:literal-expression
               sm:not*
               sm:not-expression?
               sm:not-expression
               sm:print-expressions
               sm:set
               sm:set*
               sm:set?
               sm:set-elements
               sm:var
               sm:var*
               sm:member*
               sm:var?
               sm:var-name
               sm:var-member?))

;;; Commentary:
;;;
;;; This module provides a high-level mechanism to define processes.
;;;
;;; Try: C-c C-a (sm:process (name "stay") (statement "delta")) RET
;;;
;;; Code:

;;;
;;; Type.
;;;
(define-record-type* <sm:type>
  sm:type make-sm:type
  sm:type?
  (name      sm:type-name)                 ;string
  (entities  sm:type-entities))            ;list of <string>

(define (print-sm:type type port)
  "Write a concise representation of TYPE to PORT."
  (match type
    (($ <sm:type> name entities)
     (simple-format port "sort ~a = struct " name)
     (display-join entities port "\n | ")
     (display ";" port))))

(define (sm:type< . rest)
  (apply string< (map sm:type-name rest)))


;;;
;;; Equation.
;;;
(define-record-type* <sm:equation>
  sm:equation make-sm:equation
  sm:equation?
  (left  sm:equation-left)
  (right sm:equation-right))

(define (print-sm:equation equation port)
  "Write a concise representation of EQUATION to PORT."
  (match equation
    (($ <sm:equation> left right)
     (simple-format port "eqn ~a = ~a;" left right))))

(define (equation< . rest)
  (apply string< (map sm:equation-left rest)))


;;;
;;; Entity.
;;;
(define-record-type* <sm:entity>
  sm:entity make-sm:entity
  sm:entity?
  (name     sm:entity-name)                    ;string
  (formals  sm:entity-formals
            (default #f)))

(define (print-sm:entity entity port)
  "Write a concise representation of ENTITY to PORT."
  (match entity
    (($ <sm:entity> name #f)
     (simple-format port "~a" name))
    (($ <sm:entity> name formals)
     (simple-format port "~a (" name)
     (display-join formals port ", ")
     (display ")" port))))


;;;
;;; Predicate.
;;;
(define-record-type* <sm:predicate>
  sm:predicate make-sm:predicate
  sm:predicate?
  (type sm:predicate-type)              ;string
  (name sm:predicate-name))                ;string

(define (print-sm:predicate predicate port)
  "Write a concise representation of PREDICATE to PORT."
  (match predicate
    (($ <sm:predicate> type name)
     (simple-format port "~a ? ~a" type name))))


;;;
;;; Formal.
;;;
(define (print-sm:formal formal port)
  "Write a concise representation of FORMAL to PORT."
  (match formal
    (($ <sm:formal> #f name)
     (simple-format port "~a" name))
    (($ <sm:formal> type name)
     (let ((type (if (sm:type? type) (sm:type-name type)
                     type)))
       (simple-format port "~a: ~a" name type)))))


;;;
;;; Goto.
;;;
(define-record-type* <sm:goto>
  sm:goto make-sm:goto
  sm:goto?
  (name      sm:goto-name)              ;string
  (arguments sm:goto-arguments          ;list of <string>
             (default #f)))

(define (print-sm:goto goto port)
  "Write a concise representation of GOTO to PORT."
  (match goto
    (($ <sm:goto> name #f)
     (display name port))
    (($ <sm:goto> name arguments)
     (display name port)
     (display " (" port)
     (display-join arguments port ", ")
     (display ")" port))))


;;;
;;; Event.
;;;
(define-record-type* <sm:event>
  sm:event make-sm:event
  sm:event?
  (type        sm:event-type               ;<sm:type>
               (default #f))
  (constructor sm:event-constructor
               (default #f))
  (arguments   sm:event-arguments
               (default #f)))

(define (sm:event-type=? a b)
  (eq? (sm:event-type a) (sm:event-type b)))

(define (sm:event-constructor=? a b)
  (equal? (sm:event-constructor a) (sm:event-constructor b)))

(define (sm:event-arguments=? a b)
  (equal? (sm:event-arguments a) (sm:event-arguments b)))

(define (event=? a b)
  (and (sm:event-type=? a b)
       (sm:event-constructor=? a b)
       (sm:event-arguments=? a b)))

(define (print-sm:event event port)
  "Write a concise representation of EVENT to PORT."
  (match event
    (($ <sm:event> type #f arguments)
     (display-join arguments port ", "))
    (($ <sm:event> type constructor #f)
     (simple-format port "~a" constructor))
    (($ <sm:event> type constructor arguments)
     (simple-format port "~a (" constructor)
     (display-join arguments port ", ")
     (display ")" port))))


;;;
;;; Nextion.
;;;
(define-record-type* <sm:action>
  sm:action make-sm:action
  sm:action?
  (prefix sm:action-prefix)               ;string
  (event  sm:action-event                 ;<sm:event>
          (default #f)))

(define (sm:action< a b)
  (string< (sm:action-prefix a) (sm:action-prefix b)))

(define (sm:action-prefix=? a b)
  (equal? (sm:action-prefix a) (sm:action-prefix b)))

(define (sm:action-event=? a b)
  (eq? (sm:action-event a) (sm:action-event b)))

(define (sm:action=? a b)
  (and (sm:action-prefix=? a b)
       (sm:action-event=? a b)))

(define (print-sm:action action port)
  "Write a concise representation of NEXTION to PORT."
  (match action
    (($ <sm:action> prefix #f)
     (simple-format port "~a" prefix))
    (($ <sm:action> prefix event)
     (simple-format port "~a (~a)" prefix event))))

(define %sm:delta
  (sm:goto (name "delta")))

(define %sm:bool
  (sm:type (name "Bool") (entities '())))

(define %sm:int
  (sm:type (name "Int") (entities '())))

(define %sm:nat
  (sm:type (name "Nat") (entities '())))

(define %sm:builtin-types
  (list %sm:bool %sm:delta %sm:int %sm:nat))


;;;
;;; Process.
;;;
(define-record-type* <sm:process>
  sm:process make-sm:process
  sm:process?
  (name       sm:process-name)              ;string
  (formals    sm:process-formals
              (default '()))
  (statement  sm:process-statement))        ;string

(define (print-sm:process process port)
  "Write a concise representation of PROCESS to PORT."
  (match process
    (($ <sm:process> name formals statement)
     (simple-format port "proc ~a" name)
     (display-join formals port ", " 'infix " (" 'pre ")" 'post)
     (display (if (process-line-break? statement) "\n " " ") port)
     (simple-format port "= ~a;" statement))))

(define (sm:process< . rest)
  (apply string< (map sm:process-name rest)))

(define (sm:process-actions process)
  (match process
    (($ <sm:action>) (list process))
    (($ <sm:allow> process events) (append-map sm:process-actions events))
    (($ <sm:block> process events) (append-map sm:process-actions events))
    (($ <sm:if> expression then otherwise) (append
                                            (sm:process-actions then)
                                            (sm:process-actions otherwise)))
    (($ <sm:comm> process events) (append-map sm:process-actions events))
    (($ <sm:comm-event> from to) (append (sm:process-actions from)
                                         (sm:process-actions (to process))))
    (($ <sm:hide> process events) (append-map sm:process-actions events))
    (($ <sm:multi-event> events) events)
    (($ <sm:parallel> processes) (append-map sm:process-actions processes))
    (($ <sm:process> name formals statement) (sm:process-actions statement))
    (($ <sm:rename> process events) (append-map sm:process-actions events))
    (($ <sm:rename-event> from to) (append (sm:process-actions from)
                                           (sm:process-actions to)))
    (($ <sm:mcrl2> types equations actions processes) (append-map
                                                       sm:process-actions
                                                       processes))
    (($ <sm:sequence> statements) (append-map sm:process-actions statements))
    (($ <sm:sum> var type statement) (sm:process-actions statement))
    (($ <sm:union> statements) (append-map sm:process-actions statements))
    (_ '())))


;;;
;;; Parallel.
;;;
(define-record-type* <sm:parallel>
  sm:parallel make-sm:parallel
  sm:parallel?
  (processes sm:parallel-processes))

(define-syntax sm:parallel*
  (lambda (x)
    "Return an instance of <sm:parallel>."
    (syntax-case x ()
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:parallel (list arg))
                 (make-sm:parallel arg)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:parallel arg)))))))

(define (print-sm:parallel parallel port)
  "Write a concise representation of PARALLEL to PORT."
  (match parallel
    (($ <sm:parallel> processes)
     (display-join processes port " || "))))


;;;
;;; Sequence.
;;;
(define-record-type* <sm:sequence>
  sm:sequence make-sm:sequence
  sm:sequence?
  (statements sm:sequence-statements))

(define-syntax sm:sequence*
  (lambda (x)
    "Return an instance of <sm:sequence>."
    (syntax-case x ()
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:sequence (list arg))
                 (make-sm:sequence arg)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:sequence arg)))))))

(define (print-sm:sequence sequence port)
  "Write a concise representation of SEQUENCE to PORT."
  (match sequence
    (($ <sm:sequence> statements)
     (display-join* statements #:port port #:grammar '(" . " infix)
                    #:display-element
                    (match-lambda*
                      (((and ($ <sm:union>) statement) port)
                       (display "(" port)
                       (display statement port)
                       (display ")" port))
                      ((statement port) (display statement port)))))))


;;;
;;; Union.
;;;
(define-record-type* <sm:union>
  sm:union make-sm:union
  sm:union?
  (statements sm:union-statements))

(define-syntax sm:union*
  (lambda (x)
    "Return an instance of <sm:union>."
    (syntax-case x ()
      ((_ statement)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg statement))
             (if (atom? arg) (make-sm:union (list arg))
                 (make-sm:union arg)))))
      ((_ statement ...)
       (with-syntax ((arg (datum->syntax x 'arg)))
         #'(let ((arg (list statement ...)))
             (make-sm:union arg)))))))

(define (print-sm:union union port)
  "Write a concise representation of UNION to PORT."
  (match union
    (($ <sm:union> statements)
     (display-join statements port "\n + "))))


;;;
;;; Not-expression.
;;;
(define (print-sm:not not port)
  "Write a concise representation of NOT* to PORT."
  (match not
    (($ <sm:not> expression)
     (display "!" port)
     (display expression port))))


;;;
;;; Multi-event.
;;;
(define-record-type* <sm:multi-event>
  sm:multi-event make-sm:multi-event
  sm:multi-event?
  (events sm:multi-event-events
          (default '())))

(define (print-sm:multi-event multi-event port)
  "Write a concise representation of MULTI-EVENT to PORT."
  (match multi-event
    (($ <sm:multi-event> events)
     (let* ((events (map (match-lambda
                           (($ <sm:action> prefix) prefix)
                           (name name))
                         events))
            (events (match events
                      ((name) (list name name))
                      ((h t ...) events))))
       (simple-format port "~a" (string-join events " | " 'infix))))))


;;;
;;; Comm-event.
;;;
(define-record-type* <sm:comm-event>
  sm:comm-event make-sm:comm-event
  sm:comm-event?
  (from sm:comm-event-from)                ;<sm:multi-event>
  (to   sm:comm-event-to                     ;string
        (thunked)
        (default (sm:comm-event-ticked-to this-record))))

(define (sm:comm-event-ticked-to comm-event)
  (match comm-event
    (($ <sm:comm-event> (and ($ <sm:multi-event> (event rest ...)) from) to)
     (let ((name (match event
                   (($ <sm:action> prefix) prefix)
                   (name name)))
           (type (match event
                   (($ <sm:action> prefix ($ <sm:event> type)) type)
                   (_ #f))))
       (make-sm:action (tick name)
                       (make-sm:event type #f #f))))))

(define (print-sm:comm-event comm-event port)
  "Write a concise representation of COMM-EVENT to PORT."
  (match comm-event
    (($ <sm:comm-event> from to)
     (let ((to (match (to comm-event)
                 (($ <sm:action> prefix) prefix)
                 (name name))))
       (display from port)
       (simple-format port " -> ~a" to)))))


;;;
;;; Comm.
;;;
(define-record-type* <sm:comm>
  sm:comm make-sm:comm
  sm:comm?
  (process sm:comm-process)
  (events  sm:comm-events))

(define (print-sm:comm comm port)
  "Write a concise representation of COMM to PORT."
  (match comm
    (($ <sm:comm> process events)
     (let ((name (sm:process-name process)))
       (display "comm ({\n   " port)
       (display-join events port "\n , " 'infix)
       (simple-format port "\n }, ~a)" name)))))


;;;
;;; Allow.
;;;
(define-record-type* <sm:allow>
  sm:allow make-sm:allow
  sm:allow?
  (process sm:allow-process)
  (events  sm:allow-events))

(define (print-sm:allow allow port)
  "Write a concise representation of ALLOW to PORT."
  (match allow
    (($ <sm:allow> process events)
     (let* ((name (match process
                    (($ <sm:process> name formals)
                     name)
                    (name name)))
            (comm-events
             (match process
               (($ <sm:process> name formals
                   ($ <sm:comm> comm-process comm-events))
                comm-events)
               (_ '())))
            (comm-events (map sm:comm-event-to comm-events))
            (events (append comm-events events))
            (events (map (match-lambda
                           (($ <sm:action> prefix) prefix)
                           (name name))
                         events)))
       (display "allow ({\n   " port)
       (display-join events port "\n , " 'infix)
       (simple-format port "\n }, ~a)" name)))))


;;;
;;; Block.
;;;
(define-record-type* <sm:block>
  sm:block make-sm:block
  sm:block?
  (process sm:block-process)
  (events  sm:block-events))

(define (print-sm:block block port)
  "Write a concise representation of BLOCK to PORT."
  (match block
    (($ <sm:block> process events)
     (let* ((name (match process
                    (($ <sm:process> name formals)
                     name)
                    (name name)))
            (comm-events (match process
                           (($ <sm:process> name formals
                               ($ <sm:comm> comm-process comm-events))
                            comm-events)
                           (_ '())))
            (comm-events (map sm:comm-event-to comm-events))
            (events (append comm-events events))
            (events (map (match-lambda
                           (($ <sm:action> prefix) prefix)
                           (name name))
                         events)))
       (display "block ({\n   " port)
       (display-join events port "\n , " 'infix)
       (simple-format port "\n }, ~a)" name)))))


;;;
;;; Rename-event.
;;;
(define-record-type* <sm:rename-event>
  sm:rename-event make-sm:rename-event
  sm:rename-event?
  (from sm:rename-event-from)                ;<sm:multi-event>
  (to   sm:rename-event-to
        (default #f)))

(define (sm:transpose-tick event)
  (match event
    (($ <sm:action> prefix event)
     (sm:action (prefix (sm:transpose-tick prefix))
                (event event)))
    ((and (? string?) (? (cute string-suffix? "'" <>)))
     (untick event))
    ((? string?)
     (tick event))))

(define (sm:rename-event-transpose-ticked rename-event)
  (match rename-event
    (($ <sm:rename-event> from to)
     (sm:transpose-tick from))))

(define (print-sm:rename-event rename-event port)
  "Write a concise representation of RENAME-EVENT to PORT."
  (match rename-event
    (($ <sm:rename-event> from to)
     (let* ((from (match from
                    (($ <sm:action> prefix) prefix)
                    (name name)))
            (to (match to
                  (#f (sm:transpose-tick from))
                  (($ <sm:action> prefix) prefix)
                  (name name))))
       (simple-format port "~a -> ~a" from to)))))


;;;
;;; Rename.
;;;
(define-record-type* <sm:rename>
  sm:rename make-sm:rename
  sm:rename?
  (process sm:rename-process)
  (events  sm:rename-events))

(define (print-sm:rename rename port)
  "Write a concise representation of RENAME to PORT."
  (match rename
    (($ <sm:rename> process events)
     (let* ((name (match process
                    (($ <sm:process> name) name)
                    (name name)))
            (explicit-from (filter-map
                            (match-lambda
                              (($ <sm:rename-event> ($ <sm:action> prefix)) prefix)
                              (_ #f))
                            events))
            (events (filter-map
                     (match-lambda
                       ((and event ($ <sm:comm-event> from to))
                        (let* ((rename-from (to event))
                               (rename-from (match rename-from
                                              (($ <sm:action> prefix) prefix)
                                              (name name)))
                               (rename-to
                                (match from
                                  (($ <sm:multi-event> (event rest ...)) event)
                                  (event event)))
                               (rename-to (match rename-to
                                            (($ <sm:action> prefix) prefix)
                                            (name name))))
                          (and (not (member rename-from explicit-from))
                               (sm:rename-event (from rename-from) (to rename-to)))))
                       (event event))
                     events)))
       (display "rename ({\n   " port)
       (display-join events port "\n , " 'infix)
       (simple-format port "\n }, ~a)" name)))))


;;;
;;; Hide.
;;;
(define-record-type* <sm:hide>
  sm:hide make-sm:hide
  sm:hide?
  (process sm:hide-process)
  (events  sm:hide-events))

(define (print-sm:hide hide port)
  "Write a concise representation of HIDE to PORT."
  (match hide
    (($ <sm:hide> process events)
     (let* ((name (sm:process-name process))
            (events (map (match-lambda
                           (($ <sm:action> prefix) prefix)
                           (name name))
                         events)))
       (simple-format port "hide ({
   ~a
 }, ~a)"
                      (string-join events "\n , " 'infix)
                      name)))))


;;;
;;; Sum.
;;;
(define-record-type* <sm:sum>
  sm:sum make-sm:sum
  sm:sum?
  (var       sm:sum-var
             (default "i"))
  (type      sm:sum-type)
  (statement sm:sum-statement))

(define* (sm:invoke a #:optional (args "i") #:key keep-constructor?)
  (let ((args (match args
                ((arg rest ...) args)
                (a (list a)))))
    (match a
      (($ <sm:action> prefix ($ <sm:event> type constructor))
       (sm:action (prefix prefix)
                  (event (sm:event (type type)
                                   (constructor (and keep-constructor? constructor))
                                   (arguments args)))))
      (($ <sm:predicate> type name)
       (sm:action (prefix name)
                  (event (sm:event (arguments args))))))))

(define* (sm:construct a #:optional (args "i") #:key keep-constructor?)
  (let ((args (match args
                ((arg rest ...) args)
                (a (list a)))))
    (match a
      (($ <sm:action> prefix ($ <sm:event> type constructor))
       (sm:action (prefix constructor)
                  (event (sm:event (type type)
                                   (constructor (and keep-constructor? constructor))
                                   (arguments args)))))
      (($ <sm:predicate> type name)
       (sm:action (prefix type)
                  (event (sm:event (arguments args))))))))

(define (print-sm:sum sum port)
  "Write a concise representation of SUM to PORT."
  (match sum
    (($ <sm:sum> var ($ <sm:type> name) statement)
     (simple-format port "sum ~a: ~a . ~a" var name statement))
    (($ <sm:sum> var ($ <sm:predicate> type name) statement)
     (simple-format port "sum ~a: ~a . ~a" var type statement))))


;;;
;;; If.
;;;
(define (print-sm:if sm:if port)
  "Write a concise representation of IF* to PORT."
  (match sm:if
    (($ <sm:if> expression then #f)
     (display "((" port)
     (display expression port)
     (display ") -> " port)
     (match then
       (($ <sm:union>) (simple-format port "(~a)" then))
       (_ (display then port)))
     (display ")" port))
    (($ <sm:if> expression then else)
     (display "((" port)
     (display expression port)
     (display ")\n   -> " port)
     (match then
       (($ <sm:union>) (simple-format port "(~a)" then))
       (_ (display then port)))
     (display "\n   <> " port)
     (match else
       (($ <sm:union>) (simple-format port "(~a)" else))
       (_ (display else port)))
     (display ")" port))))


;;;
;;; Scmackerel.
;;;
(define-record-type* <sm:mcrl2>
  sm:mcrl2 make-sm:mcrl2
  sm:mcrl2?
  (types     sm:mcrl2-types
             (default '()))
  (equations sm:mcrl2-equations
             (default '()))
  (actions   sm:mcrl2-actions
             (default #f))
  (processes sm:mcrl2-processes
             (default '()))
  (init      sm:mcrl2-init
             (default #f)))

(define (print-sm:mcrl2 scmackerel port)
  "Write a concise representation of SCMACKEREL to PORT."
  (set-record-printers!)
  (catch #t
    (lambda _
      (match scmackerel
        (($ <sm:mcrl2> types equations actions processes init)
         (let* ((actions (or actions
                             (append-map sm:process-actions processes)))
                (actions (filter (negate (cute memq <> %sm:builtin-types)) actions)))
           (when (pair? equations)
             (for-each (cute simple-format port "~a\n" <>) equations))
           (when (pair? types)
             (for-each (cute simple-format port "~a\n" <>) types))
           (let ((typed untyped (partition
                                 (conjoin sm:action?
                                          sm:action-event
                                          (compose sm:event-type sm:action-event))
                                 actions)))
             (when (pair? untyped)
               (let* ((untyped (filter sm:action? untyped))
                      (untyped-names (map sm:action-prefix untyped))
                      (untyped-names (sort untyped-names string<?))
                      (untyped-names (delete-duplicates untyped-names)))
                 (simple-format port "act ~a;\n" (string-join untyped-names ","))))
             (let* ((typed (filter sm:action? typed))
                    (typed (sort typed sm:action<))
                    (typed (delete-duplicates typed sm:action-prefix=?)))
               (for-each
                (lambda (a)
                  (match a
                    (($ <sm:action> prefix ($ <sm:event> type))
                     (let ((type (match type
                                   ((and (h t ...) types)
                                    (string-join (map sm:type-name types) " # "))
                                   (type (sm:type-name type)))))
                       (simple-format port "act ~a:~a;\n" prefix type)))))
                typed))
             (display-join processes port "\n" 'suffix))
           (when init
             (simple-format port "init ~a;\n" (sm:process-name init)))))))
    (lambda (key . args)
      (reset-record-printers!)
      (apply throw key args))))

(set-record-type-printer! <sm:mcrl2> print-sm:mcrl2)

(define (process-line-break? statement)
  (sm:union? statement))

(define (set-record-printers!)
  ((@@ (scmackerel expressions) set-record-printers!))
  (set-record-type-printer! <sm:action> print-sm:action)
  (set-record-type-printer! <sm:allow> print-sm:allow)
  (set-record-type-printer! <sm:block> print-sm:block)
  (set-record-type-printer! <sm:comm-event> print-sm:comm-event)
  (set-record-type-printer! <sm:comm> print-sm:comm)
  (set-record-type-printer! <sm:entity> print-sm:entity)
  (set-record-type-printer! <sm:equation> print-sm:equation)
  (set-record-type-printer! <sm:event> print-sm:event)
  (set-record-type-printer! <sm:formal> print-sm:formal)
  (set-record-type-printer! <sm:goto> print-sm:goto)
  (set-record-type-printer! <sm:hide> print-sm:hide)
  (set-record-type-printer! <sm:if> print-sm:if)
  (set-record-type-printer! <sm:multi-event> print-sm:multi-event)
  (set-record-type-printer! <sm:not> print-sm:not)
  (set-record-type-printer! <sm:parallel> print-sm:parallel)
  (set-record-type-printer! <sm:predicate> print-sm:predicate)
  (set-record-type-printer! <sm:process> print-sm:process)
  (set-record-type-printer! <sm:rename-event> print-sm:rename-event)
  (set-record-type-printer! <sm:rename> print-sm:rename)
  (set-record-type-printer! <sm:mcrl2> print-sm:mcrl2)
  (set-record-type-printer! <sm:sequence> print-sm:sequence)
  (set-record-type-printer! <sm:sum> print-sm:sum)
  (set-record-type-printer! <sm:type> print-sm:type)
  (set-record-type-printer! <sm:union> print-sm:union))

(define (reset-record-printers!)
  ((@@ (scmackerel expressions) reset-record-printers!))
  (set-record-type-printer! <sm:action> #f)
  (set-record-type-printer! <sm:allow> #f)
  (set-record-type-printer! <sm:block> #f)
  (set-record-type-printer! <sm:comm-event> #f)
  (set-record-type-printer! <sm:comm> #f)
  (set-record-type-printer! <sm:entity> #f)
  (set-record-type-printer! <sm:event> #f)
  (set-record-type-printer! <sm:formal> #f)
  (set-record-type-printer! <sm:goto> #f)
  (set-record-type-printer! <sm:hide> #f)
  (set-record-type-printer! <sm:if> #f)
  (set-record-type-printer! <sm:multi-event> #f)
  (set-record-type-printer! <sm:not> #f)
  (set-record-type-printer! <sm:parallel> #f)
  (set-record-type-printer! <sm:predicate> #f)
  (set-record-type-printer! <sm:process> #f)
  (set-record-type-printer! <sm:rename-event> #f)
  (set-record-type-printer! <sm:rename> #f)
  (set-record-type-printer! <sm:mcrl2> #f)
  (set-record-type-printer! <sm:sequence> #f)
  (set-record-type-printer! <sm:sum> #f)
  (set-record-type-printer! <sm:type> #f)
  (set-record-type-printer! <sm:union> #f))

(define* (sm:print-mcrl2 mcrl2 #:optional (port (current-output-port)))
  (set-record-printers!)
  (catch #t
    (lambda _ (display mcrl2 port))
    (lambda (key . args)
      (reset-record-printers!)
      (apply throw key args))))

(define (sm:mcrl2->string mcrl2)
  (with-output-to-string
    (cute sm:print-mcrl2 mcrl2)))


;;;
;;; Generate mCRL2.
;;;
(define* (sm:module->mcrl2 #:optional (module (current-module)) #:key init)
  (let* ((variables (module-map cons module))
         (values (map (compose variable-ref cdr) variables))
         (types (sort (filter sm:type? values) sm:type<))
         (equations (sort (filter sm:equation? values) equation<))
         (processes (sort (filter sm:process? values) sm:process<)))
    (sm:mcrl2 (types types)
              (equations equations)
              (actions (append-map sm:process-actions processes))
              (processes processes)
              (init init))))

(define* (sm:module-print-mcrl2 init #:key (module (current-module)) (port (current-output-port)))
  (print-sm:mcrl2 (sm:module->mcrl2 module #:init init) port))
