#! /bin/sh
# SCMackerel --- A GNU Guile front-end for mCRL2
# Copyright © 2022,2024 Janneke Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of SCMackerel.
#
# SCMackerel is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# SCMackerel is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SCMackerel.  If not, see <http://www.gnu.org/licenses/>.

LANG=en_US.UTF-8
export LANG
for E in $(set | grep -oE LC_[^=]+); do unset $E; done

autoreconf -ifv

# Replace Automake's build-aux/mdate-sh with build-aux/mdate-from-git, our
# own, reproducible version.
chmod +w build-aux/mdate-sh
rm -f build-aux/mdate-sh
ln -s mdate-from-git.scm build-aux/mdate-sh
