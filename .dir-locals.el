;; The GNU project defaults.  These are also the GNU Emacs defaults.
;; Re-asserting theme here, however, as a courtesy for setups that use
;; a global override.
((makefile-mode . ((indent-tabs-mode . t)))

 (nil . ((indent-tabs-mode . nil)
         (fill-column . 72)
         (eval
          .
          (progn
            (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)))))

 (diff-mode . (eval (progn (remove-hook 'before-save-hook 'delete-trailing-whitespace t))))

 (scheme-mode
  .
  ((geiser-active-implementations . (guile))
   (eval
    .
    (progn
      (defun prefix-dir-locals-dir (elt)
        (concat (locate-dominating-file buffer-file-name ".dir-locals.el") elt))
      (mapcar
       (lambda (dir) (add-to-list 'geiser-guile-load-path dir))
       (mapcar
        #'prefix-dir-locals-dir
        '(".")))))

   ;; SCMackerel
   (eval . (put 'sm:process 'scheme-indent-function 0))
   (eval . (put 'sm:mcrl2 'scheme-indent-function 0))
   (eval . (put 'sm:code 'scheme-indent-function 0))
   (eval . (put 'sm:header 'scheme-indent-function 0))
   (eval . (put 'sm:struct 'scheme-indent-function 0))))

 (texinfo-mode    . ((indent-tabs-mode . nil)
                     (fill-column . 72))))
